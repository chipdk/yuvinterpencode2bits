import numpy as np
import caffe
import os
import h5py


#Create mean image function
def create_mean():
    linkimages = '/home/chipdk/trainingimages_createmean/832x480/'
    list_of_images = os.listdir(linkimages)
	
    #for i in range(0,len(list_of_images)):	
    for i in range(0,1):	
        filename = str(list_of_images[i])
        linkimgs = linkimages+filename
        print linkimgs
        if i == 0:
            n = np.int32(np.loadtxt(linkimgs))/255;
        else:
            n = n + np.int32(np.loadtxt(linkimgs))/255;
    

    return np.uint8(np.double(n)/len(list_of_images))

	


#paths out of textfile,here to simplify as an array , usually comes out of a txt file 
#but that's not the issue
avg_img  = create_mean()
print avg_img.shape
print type(avg_img)
print avg_img

height, width = avg_img.shape
meanimg = np.zeros((1,height*4+32, width*4+32), dtype=np.float32) # K = channels, H = height, W = width
print meanimg.shape
print meanimg
np.save('mean832x480.npy', meanimg)


mean_binproto = 'mean832x480.binaryproto' 
blob = caffe.io.array_to_blobproto(meanimg)
print blob.shape
with open( mean_binproto, 'wb' ) as f :
    f.write( blob.SerializeToString())

	