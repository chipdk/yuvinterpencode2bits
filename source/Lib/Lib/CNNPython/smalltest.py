import caffe
import numpy as np
from caffe.proto import caffe_pb2  # noqa
from google.protobuf import text_format

def get_transformer(net, deploy_file, mean_file=None):
    """
    Returns an instance of caffe.io.Transformer
    Arguments:
    deploy_file -- path to a .prototxt file
    Keyword arguments:
    mean_file -- path to a .binaryproto file (optional)
    """

    t = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    t.set_transpose('data', (2, 0,1))  # transpose to (channels, height, width)

    if mean_file:
        # set mean pixel
        with open(mean_file) as infile:
            blob = caffe_pb2.BlobProto()
            blob.MergeFromString(infile.read())
            if blob.HasField('shape'): 
                blob_dims = blob.shape
                print 'blob.shape'
                print blob.shape
                assert len(blob_dims) == 4, 'Shape should have 4 dimensions - shape is "%s"' % blob.shape
            elif blob.HasField('num') and blob.HasField('channels') and \
                    blob.HasField('height') and blob.HasField('width'):
                blob_dims = (blob.num, blob.channels, blob.height, blob.width)
            else:
                raise ValueError('blob does not provide shape or 4d dimensions')
            pixel = np.reshape(blob.data, blob_dims[1:]).mean(1).mean(1)
            t.set_mean('data', pixel)   
    return t

def forward_pass(images, net, transformer, batch_size=1):
    """
    Returns scores for each image as an np.ndarray (nImages x nClasses)
    Arguments:
    images -- a list of np.ndarrays
    net -- a caffe.Net
    transformer -- a caffe.io.Transformer
    Keyword arguments:
    batch_size -- how many images can be processed at once
        (a high value may result in out-of-memory errors)
    """
    print images.ndim
    caffe_images = []
    for image in images:
        if image.ndim == 2:
            caffe_images.append(image[:,:,np.newaxis])
        else:
            caffe_images.append(image)

    caffe_images = np.array(caffe_images)

    dims = transformer.inputs['data'][1:]

    for chunk in [caffe_images[x:x+batch_size] for x in xrange(0, len(caffe_images), batch_size)]:
        new_shape = (len(chunk),) + tuple(dims)
        if net.blobs['data'].data.shape != new_shape:
            net.blobs['data'].reshape(*new_shape)
        for index, image in enumerate(chunk):
            image_data = transformer.preprocess('data', image)
            net.blobs['data'].data[index] = image_data
			
        nb = net.blobs # nb is an OrderedDict of the blob objects that make up a VGG16 net
        for ctr, name in enumerate(nb): 
            print ctr, name, nb[name].data.shape
        num,ch,hei,wid = net.blobs['conv3'].data.shape
        net.blobs['label'].reshape(net.blobs['data'].data.shape[0],1,hei,wid)
        print net.blobs['conv3'].data
        print 'forward'
        net.forward()  
        print 'after forward'
        print net.blobs['conv3'].data
    return net.blobs['conv3'].data
	               
################
####load net####
################
caffe_root = '~/caffe/'
model_root = '/home/chipdk/CNN_interpolating_model/'
deploy_file = model_root + 'SRCNN_net.prototxt'
caffemodel = model_root + 'DCTIF_OR_frame_32x32to32x32_q22_iter_15000000.caffemodel'
net = caffe.Net(deploy_file, caffemodel, caffe.TEST)
mean_file = './mean832x480.npy'
caffe.set_mode_cpu()


##################
####load image####
##################
data = np.loadtxt('/home/chipdk/backuphmicnn/build/linux/CNNinput/BQMall_832x480_60q22_0.txt');
input_float1 = data/255
input_float = input_float1
height,width = input_float.shape
batch_size = 128

im_input1 = input_float[np.newaxis, :,:]

im_input = np.zeros((batch_size, height,width),type(input_float[0,0]))

for i in range(batch_size):
    im_input[i,:,:] = input_float

CNNshape = input_float[np.newaxis,np.newaxis, :,:]
print im_input.shape
nb = net.blobs # nb is an OrderedDict of the blob objects that make up a VGG16 net
for ctr, name in enumerate(nb): 
    print ctr, name, nb[name].data.shape
##############################
####reshape net data input####
##############################
net.blobs['data'].reshape(batch_size, 1, height, width)
nb = net.blobs # nb is an OrderedDict of the blob objects that make up a VGG16 net


parsible_net = caffe_pb2.NetParameter()
text_format.Merge(open(deploy_file).read(), parsible_net)
prev_conv_width = width
prev_conv_height = height

for layer in parsible_net.layer:
    if layer.type == 'Convolution':
        kernel = layer.convolution_param.kernel_size[0] if len(layer.convolution_param.kernel_size) else 1
        stride = layer.convolution_param.stride[0] if len(layer.convolution_param.stride) else 1
        pad    = layer.convolution_param.pad[0] if len(layer.convolution_param.pad) else 0
        after_conv_width = (prev_conv_width-kernel+2*pad)/stride+1
        after_conv_height = (prev_conv_height-kernel+2*pad)/stride+1
        net.blobs[layer.name].reshape(batch_size,64, after_conv_height,after_conv_width)
        prev_conv_height=after_conv_height
        prev_conv_width = after_conv_width
net.blobs['label'].reshape(batch_size, 1, prev_conv_height, prev_conv_width)
#net.blobs['label'].reshape(*CNNshape.shape)    
nb = net.blobs # nb is an OrderedDict of the blob objects that make up a VGG16 net
for ctr, name in enumerate(nb): 
   print ctr, name, nb[name].data.shape

########################
####feed data to net####
########################

transformer = get_transformer(net, deploy_file)
score = forward_pass(im_input, net, transformer,batch_size)
print score






































