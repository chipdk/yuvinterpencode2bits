import numpy as np
import lmdb
import caffe
import sys

N = 1000
height = 480
width = 832
# Let's pretend this is interesting data
X = np.zeros((N, 1, height, width), dtype=np.float32)
y = np.zeros((N, 1, height, width), dtype=np.float32)

# We need to prepare the database for the size. We'll set it 10 times
# greater than what we theoretically need. There is little drawback to
# setting this too big. If you still run into problem after raising
# this, you might want to try saving fewer entries in a single
# transaction.
map_size = X.nbytes * 10

env = lmdb.open('mylmdb', map_size=map_size)

with env.begin(write=True) as txn:
    # txn is a Transaction object
    for i in range(N):
        datum = caffe.proto.caffe_pb2.Datum()
        datum.channels = X.shape[1]
        datum.height = X.shape[2]
        datum.width = X.shape[3]
        datum.data = X[i].tobytes()  # or .tostring() if numpy < 1.9
        datum.label = y[i].tobytes()
        str_id = '{:08}'.format(i) 

        # The encode is only essential in Python 3
        txn.put(str_id.encode('ascii'), datum.SerializeToString())