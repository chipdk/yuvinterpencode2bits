/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2017, ITU/ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file     TComPrediction.cpp
\brief    prediction class
*/

#include <memory.h>
#include "TComPrediction.h"
#include "TComPic.h"
#include "TComTU.h"
#include <fstream>

//! \ingroup TLibCommon
//! \{

// ====================================================================================================================
// Tables
// ====================================================================================================================

const UChar TComPrediction::m_aucIntraFilter[MAX_NUM_CHANNEL_TYPE][MAX_INTRA_FILTER_DEPTHS] =
{
	{ // Luma
		10, //4x4
		7, //8x8
		1, //16x16
		0, //32x32
		10, //64x64
	},
	{ // Chroma
		10, //4xn
		7, //8xn
		1, //16xn
		0, //32xn
		10, //64xn
	}

};

// ====================================================================================================================
// Constructor / destructor / initialize
// ====================================================================================================================

TComPrediction::TComPrediction()
	: m_pLumaRecBuffer(0)
	, m_iLumaRecStride(0)
{
	for (UInt ch = 0; ch<MAX_NUM_COMPONENT; ch++)
	{
		for (UInt buf = 0; buf<2; buf++)
		{
			m_piYuvExt[ch][buf] = NULL;
		}
	}
}

TComPrediction::~TComPrediction()
{
	destroy();
}

Void TComPrediction::destroy()
{
	for (UInt ch = 0; ch<MAX_NUM_COMPONENT; ch++)
	{
		for (UInt buf = 0; buf<NUM_PRED_BUF; buf++)
		{
			delete[] m_piYuvExt[ch][buf];
			m_piYuvExt[ch][buf] = NULL;
		}
	}

	for (UInt i = 0; i<NUM_REF_PIC_LIST_01; i++)
	{
		m_acYuvPred[i].destroy();
	}

	m_cYuvPredTemp.destroy();

	if (m_pLumaRecBuffer)
	{
		delete[] m_pLumaRecBuffer;
		m_pLumaRecBuffer = 0;
	}
	m_iLumaRecStride = 0;

	for (UInt i = 0; i < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; i++)
	{
		for (UInt j = 0; j < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; j++)
		{
			m_filteredBlock[i][j].destroy();
		}
		m_filteredBlockTmp[i].destroy();
	}
}

Void TComPrediction::initTempBuff(ChromaFormat chromaFormatIDC)
{
	// if it has been initialised before, but the chroma format has changed, release the memory and start again.
	if (m_piYuvExt[COMPONENT_Y][PRED_BUF_UNFILTERED] != NULL && m_cYuvPredTemp.getChromaFormat() != chromaFormatIDC)
	{
		destroy();
	}

	if (m_piYuvExt[COMPONENT_Y][PRED_BUF_UNFILTERED] == NULL) // check if first is null (in which case, nothing initialised yet)
	{
		Int extWidth = MAX_CU_SIZE + 16;
		Int extHeight = MAX_CU_SIZE + 1;

		for (UInt i = 0; i < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; i++)
		{
			m_filteredBlockTmp[i].create(extWidth, extHeight + 7, chromaFormatIDC);
			for (UInt j = 0; j < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; j++)
			{
				m_filteredBlock[i][j].create(extWidth, extHeight, chromaFormatIDC);
			}
		}

		m_iYuvExtSize = (MAX_CU_SIZE * 2 + 1) * (MAX_CU_SIZE * 2 + 1);
		for (UInt ch = 0; ch<MAX_NUM_COMPONENT; ch++)
		{
			for (UInt buf = 0; buf<NUM_PRED_BUF; buf++)
			{
				m_piYuvExt[ch][buf] = new Pel[m_iYuvExtSize];
			}
		}

		// new structure
		for (UInt i = 0; i<NUM_REF_PIC_LIST_01; i++)
		{
			m_acYuvPred[i].create(MAX_CU_SIZE, MAX_CU_SIZE, chromaFormatIDC);
		}

		m_cYuvPredTemp.create(MAX_CU_SIZE, MAX_CU_SIZE, chromaFormatIDC);
	}


	if (m_iLumaRecStride != (MAX_CU_SIZE >> 1) + 1)
	{
		m_iLumaRecStride = (MAX_CU_SIZE >> 1) + 1;
		if (!m_pLumaRecBuffer)
		{
			m_pLumaRecBuffer = new Pel[m_iLumaRecStride * m_iLumaRecStride];
		}
	}
}

// ====================================================================================================================
// Public member functions
// ====================================================================================================================

// Function for calculating DC value of the reference samples used in Intra prediction
//NOTE: Bit-Limit - 25-bit source
Pel TComPrediction::predIntraGetPredValDC(const Pel* pSrc, Int iSrcStride, UInt iWidth, UInt iHeight)
{
	assert(iWidth > 0 && iHeight > 0);
	Int iInd, iSum = 0;
	Pel pDcVal;

	for (iInd = 0; iInd < iWidth; iInd++)
	{
		iSum += pSrc[iInd - iSrcStride];
	}
	for (iInd = 0; iInd < iHeight; iInd++)
	{
		iSum += pSrc[iInd*iSrcStride - 1];
	}

	pDcVal = (iSum + iWidth) / (iWidth + iHeight);

	return pDcVal;
}

// Function for deriving the angular Intra predictions

/** Function for deriving the simplified angular intra predictions.
* \param bitDepth           bit depth
* \param pSrc               pointer to reconstructed sample array
* \param srcStride          the stride of the reconstructed sample array
* \param pTrueDst           reference to pointer for the prediction sample array
* \param dstStrideTrue      the stride of the prediction sample array
* \param uiWidth            the width of the block
* \param uiHeight           the height of the block
* \param channelType        type of pel array (luma/chroma)
* \param format             chroma format
* \param dirMode            the intra prediction mode index
* \param blkAboveAvailable  boolean indication if the block above is available
* \param blkLeftAvailable   boolean indication if the block to the left is available
* \param bEnableEdgeFilters indication whether to enable edge filters
*
* This function derives the prediction samples for the angular mode based on the prediction direction indicated by
* the prediction mode index. The prediction direction is given by the displacement of the bottom row of the block and
* the reference row above the block in the case of vertical prediction or displacement of the rightmost column
* of the block and reference column left from the block in the case of the horizontal prediction. The displacement
* is signalled at 1/32 pixel accuracy. When projection of the predicted pixel falls inbetween reference samples,
* the predicted value for the pixel is linearly interpolated from the reference samples. All reference samples are taken
* from the extended main reference.
*/
//NOTE: Bit-Limit - 25-bit source
Void TComPrediction::xPredIntraAng(Int bitDepth,
	const Pel* pSrc, Int srcStride,
	Pel* pTrueDst, Int dstStrideTrue,
	UInt uiWidth, UInt uiHeight, ChannelType channelType,
	UInt dirMode, const Bool bEnableEdgeFilters
)
{
	Int width = Int(uiWidth);
	Int height = Int(uiHeight);

	// Map the mode index to main prediction direction and angle
	assert(dirMode != PLANAR_IDX); //no planar
	const Bool modeDC = dirMode == DC_IDX;

	// Do the DC prediction
	if (modeDC)
	{
		const Pel dcval = predIntraGetPredValDC(pSrc, srcStride, width, height);

		for (Int y = height; y>0; y--, pTrueDst += dstStrideTrue)
		{
			for (Int x = 0; x<width;) // width is always a multiple of 4.
			{
				pTrueDst[x++] = dcval;
			}
		}
	}
	else // Do angular predictions
	{
		const Bool       bIsModeVer = (dirMode >= 18);
		const Int        intraPredAngleMode = (bIsModeVer) ? (Int)dirMode - VER_IDX : -((Int)dirMode - HOR_IDX);
		const Int        absAngMode = abs(intraPredAngleMode);
		const Int        signAng = intraPredAngleMode < 0 ? -1 : 1;
		const Bool       edgeFilter = bEnableEdgeFilters && isLuma(channelType) && (width <= MAXIMUM_INTRA_FILTERED_WIDTH) && (height <= MAXIMUM_INTRA_FILTERED_HEIGHT);

		// Set bitshifts and scale the angle parameter to block size
		static const Int angTable[9] = { 0,    2,    5,   9,  13,  17,  21,  26,  32 };
		static const Int invAngTable[9] = { 0, 4096, 1638, 910, 630, 482, 390, 315, 256 }; // (256 * 32) / Angle
		Int invAngle = invAngTable[absAngMode];
		Int absAng = angTable[absAngMode];
		Int intraPredAngle = signAng * absAng;

		Pel* refMain;
		Pel* refSide;

		Pel  refAbove[2 * MAX_CU_SIZE + 1];
		Pel  refLeft[2 * MAX_CU_SIZE + 1];

		// Initialize the Main and Left reference array.
		if (intraPredAngle < 0)
		{
			const Int refMainOffsetPreScale = (bIsModeVer ? height : width) - 1;
			const Int refMainOffset = height - 1;
			for (Int x = 0; x<width + 1; x++)
			{
				refAbove[x + refMainOffset] = pSrc[x - srcStride - 1];
			}
			for (Int y = 0; y<height + 1; y++)
			{
				refLeft[y + refMainOffset] = pSrc[(y - 1)*srcStride - 1];
			}
			refMain = (bIsModeVer ? refAbove : refLeft) + refMainOffset;
			refSide = (bIsModeVer ? refLeft : refAbove) + refMainOffset;

			// Extend the Main reference to the left.
			Int invAngleSum = 128;       // rounding for (shift by 8)
			for (Int k = -1; k>(refMainOffsetPreScale + 1)*intraPredAngle >> 5; k--)
			{
				invAngleSum += invAngle;
				refMain[k] = refSide[invAngleSum >> 8];
			}
		}
		else
		{
			for (Int x = 0; x<2 * width + 1; x++)
			{
				refAbove[x] = pSrc[x - srcStride - 1];
			}
			for (Int y = 0; y<2 * height + 1; y++)
			{
				refLeft[y] = pSrc[(y - 1)*srcStride - 1];
			}
			refMain = bIsModeVer ? refAbove : refLeft;
			refSide = bIsModeVer ? refLeft : refAbove;
		}

		// swap width/height if we are doing a horizontal mode:
		Pel tempArray[MAX_CU_SIZE*MAX_CU_SIZE];
		const Int dstStride = bIsModeVer ? dstStrideTrue : MAX_CU_SIZE;
		Pel *pDst = bIsModeVer ? pTrueDst : tempArray;
		if (!bIsModeVer)
		{
			std::swap(width, height);
		}

		if (intraPredAngle == 0)  // pure vertical or pure horizontal
		{
			for (Int y = 0; y<height; y++)
			{
				for (Int x = 0; x<width; x++)
				{
					pDst[y*dstStride + x] = refMain[x + 1];
				}
			}

			if (edgeFilter)
			{
				for (Int y = 0; y<height; y++)
				{
					pDst[y*dstStride] = Clip3(0, ((1 << bitDepth) - 1), pDst[y*dstStride] + ((refSide[y + 1] - refSide[0]) >> 1));
				}
			}
		}
		else
		{
			Pel *pDsty = pDst;

			for (Int y = 0, deltaPos = intraPredAngle; y<height; y++, deltaPos += intraPredAngle, pDsty += dstStride)
			{
				const Int deltaInt = deltaPos >> 5;
				const Int deltaFract = deltaPos & (32 - 1);

				if (deltaFract)
				{
					// Do linear filtering
					const Pel *pRM = refMain + deltaInt + 1;
					Int lastRefMainPel = *pRM++;
					for (Int x = 0; x<width; pRM++, x++)
					{
						Int thisRefMainPel = *pRM;
						pDsty[x + 0] = (Pel)(((32 - deltaFract)*lastRefMainPel + deltaFract*thisRefMainPel + 16) >> 5);
						lastRefMainPel = thisRefMainPel;
					}
				}
				else
				{
					// Just copy the integer samples
					for (Int x = 0; x<width; x++)
					{
						pDsty[x] = refMain[x + deltaInt + 1];
					}
				}
			}
		}

		// Flip the block if this is the horizontal mode
		if (!bIsModeVer)
		{
			for (Int y = 0; y<height; y++)
			{
				for (Int x = 0; x<width; x++)
				{
					pTrueDst[x*dstStrideTrue] = pDst[x];
				}
				pTrueDst++;
				pDst += dstStride;
			}
		}
	}
}

Void TComPrediction::predIntraAng(const ComponentID compID, UInt uiDirMode, Pel* piOrg /* Will be null for decoding */, UInt uiOrgStride, Pel* piPred, UInt uiStride, TComTU &rTu, const Bool bUseFilteredPredSamples, const Bool bUseLosslessDPCM)
{
	const ChannelType    channelType = toChannelType(compID);
	const TComRectangle &rect = rTu.getRect(isLuma(compID) ? COMPONENT_Y : COMPONENT_Cb);
	const Int            iWidth = rect.width;
	const Int            iHeight = rect.height;

	assert(g_aucConvertToBit[iWidth] >= 0); //   4x  4
	assert(g_aucConvertToBit[iWidth] <= 5); // 128x128
											//assert( iWidth == iHeight  );

	Pel *pDst = piPred;

	// get starting pixel in block
	const Int sw = (2 * iWidth + 1);

	if (bUseLosslessDPCM)
	{
		const Pel *ptrSrc = getPredictorPtr(compID, false);
		// Sample Adaptive intra-Prediction (SAP)
		if (uiDirMode == HOR_IDX)
		{
			// left column filled with reference samples
			// remaining columns filled with piOrg data (if available).
			for (Int y = 0; y<iHeight; y++)
			{
				piPred[y*uiStride + 0] = ptrSrc[(y + 1)*sw];
			}
			if (piOrg != 0)
			{
				piPred += 1; // miss off first column
				for (Int y = 0; y<iHeight; y++, piPred += uiStride, piOrg += uiOrgStride)
				{
					memcpy(piPred, piOrg, (iWidth - 1) * sizeof(Pel));
				}
			}
		}
		else // VER_IDX
		{
			// top row filled with reference samples
			// remaining rows filled with piOrd data (if available)
			for (Int x = 0; x<iWidth; x++)
			{
				piPred[x] = ptrSrc[x + 1];
			}
			if (piOrg != 0)
			{
				piPred += uiStride; // miss off the first row
				for (Int y = 1; y<iHeight; y++, piPred += uiStride, piOrg += uiOrgStride)
				{
					memcpy(piPred, piOrg, iWidth * sizeof(Pel));
				}
			}
		}
	}
	else
	{
		const Pel *ptrSrc = getPredictorPtr(compID, bUseFilteredPredSamples);

		if (uiDirMode == PLANAR_IDX)
		{
			xPredIntraPlanar(ptrSrc + sw + 1, sw, pDst, uiStride, iWidth, iHeight);
		}
		else
		{
			// Create the prediction
			TComDataCU *const pcCU = rTu.getCU();
			const UInt              uiAbsPartIdx = rTu.GetAbsPartIdxTU();
			const Bool              enableEdgeFilters = !(pcCU->isRDPCMEnabled(uiAbsPartIdx) && pcCU->getCUTransquantBypass(uiAbsPartIdx));
#if O0043_BEST_EFFORT_DECODING
			const Int channelsBitDepthForPrediction = rTu.getCU()->getSlice()->getSPS()->getStreamBitDepth(channelType);
#else
			const Int channelsBitDepthForPrediction = rTu.getCU()->getSlice()->getSPS()->getBitDepth(channelType);
#endif
			xPredIntraAng(channelsBitDepthForPrediction, ptrSrc + sw + 1, sw, pDst, uiStride, iWidth, iHeight, channelType, uiDirMode, enableEdgeFilters);

			if (uiDirMode == DC_IDX)
			{
				xDCPredFiltering(ptrSrc + sw + 1, sw, pDst, uiStride, iWidth, iHeight, channelType);
			}
		}
	}

}

/** Check for identical motion in both motion vector direction of a bi-directional predicted CU
* \returns true, if motion vectors and reference pictures match
*/
Bool TComPrediction::xCheckIdenticalMotion(TComDataCU* pcCU, UInt PartAddr)
{
	if (pcCU->getSlice()->isInterB() && !pcCU->getSlice()->getPPS()->getWPBiPred())
	{
		if (pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(PartAddr) >= 0 && pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(PartAddr) >= 0)
		{
			Int RefPOCL0 = pcCU->getSlice()->getRefPic(REF_PIC_LIST_0, pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(PartAddr))->getPOC();
			Int RefPOCL1 = pcCU->getSlice()->getRefPic(REF_PIC_LIST_1, pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(PartAddr))->getPOC();
			if (RefPOCL0 == RefPOCL1 && pcCU->getCUMvField(REF_PIC_LIST_0)->getMv(PartAddr) == pcCU->getCUMvField(REF_PIC_LIST_1)->getMv(PartAddr))
			{
				return true;
			}
		}
	}
	return false;
}
Void TComPrediction::motionCompensation_Selection(TComDataCU* pcCU, TComYuv* pcYuvPred, RefPicList eRefPicList, Int iPartIdx, int YInterpType, int UVInterpType)
{
	if (YInterpType == 0 && UVInterpType == 0)//DCTIF interpolation
	{//DCTIf for Y, U and V
		Int         iWidth;
		Int         iHeight;
		UInt        uiPartAddr;
		const TComSlice *pSlice = pcCU->getSlice();
		const SliceType  sliceType = pSlice->getSliceType();
		const TComPPS   &pps = *(pSlice->getPPS());

		if (iPartIdx >= 0)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);
			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
			return;
		}

		for (iPartIdx = 0; iPartIdx < pcCU->getNumPartitions(); iPartIdx++)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);

			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
		}
		return;
	}
	else if (YInterpType == 0 && UVInterpType == 1)
	{//DCTIF for Y and CNN for U and V
		Int         iWidth;
		Int         iHeight;
		UInt        uiPartAddr;
		const TComSlice *pSlice = pcCU->getSlice();
		const SliceType  sliceType = pSlice->getSliceType();
		const TComPPS   &pps = *(pSlice->getPPS());

		if (iPartIdx >= 0)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);
			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
			return;
		}

		for (iPartIdx = 0; iPartIdx < pcCU->getNumPartitions(); iPartIdx++)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);

			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
		}
		return;
	}
	else if (YInterpType == 1 && UVInterpType == 1)
	{//CNN for Y, U and V
		Int         iWidth;
		Int         iHeight;
		UInt        uiPartAddr;
		const TComSlice *pSlice = pcCU->getSlice();
		const SliceType  sliceType = pSlice->getSliceType();
		const TComPPS   &pps = *(pSlice->getPPS());

		if (iPartIdx >= 0)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);
			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
			return;
		}

		for (iPartIdx = 0; iPartIdx < pcCU->getNumPartitions(); iPartIdx++)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);

			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
		}
		return;
	}
	else
	{//CNN for Y and DCTIF for U  and V
		Int         iWidth;
		Int         iHeight;
		UInt        uiPartAddr;
		const TComSlice *pSlice = pcCU->getSlice();
		const SliceType  sliceType = pSlice->getSliceType();
		const TComPPS   &pps = *(pSlice->getPPS());

		if (iPartIdx >= 0)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);
			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
			return;
		}

		for (iPartIdx = 0; iPartIdx < pcCU->getNumPartitions(); iPartIdx++)
		{
			pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);

			if (eRefPicList != REF_PIC_LIST_X)
			{
				if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
					xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
				else
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
				}
			}
			else
			{
				if (xCheckIdenticalMotion(pcCU, uiPartAddr))
				{
					xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
				}
				else
				{
					xPredInterBi_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
				}
			}
		}
		return;
	}
}


Void TComPrediction::motionCompensation(TComDataCU* pcCU, TComYuv* pcYuvPred, RefPicList eRefPicList, Int iPartIdx)
{
	Int         iWidth;
	Int         iHeight;
	UInt        uiPartAddr;
	const TComSlice *pSlice = pcCU->getSlice();
	const SliceType  sliceType = pSlice->getSliceType();
	const TComPPS   &pps = *(pSlice->getPPS());

	if (iPartIdx >= 0)
	{
		pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);
		if (eRefPicList != REF_PIC_LIST_X)
		{
			if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
				xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
			}
			else
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
			}
		}
		else
		{
			if (xCheckIdenticalMotion(pcCU, uiPartAddr))
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
			}
			else
			{
				xPredInterBi_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
			}
		}
		return;
	}

	for (iPartIdx = 0; iPartIdx < pcCU->getNumPartitions(); iPartIdx++)
	{
		pcCU->getPartIndexAndSize(iPartIdx, uiPartAddr, iWidth, iHeight);

		if (eRefPicList != REF_PIC_LIST_X)
		{
			if ((sliceType == P_SLICE && pps.getUseWP()) || (sliceType == B_SLICE && pps.getWPBiPred()))
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred, true);
				xWeightedPredictionUni(pcCU, pcYuvPred, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
			}
			else
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcYuvPred);
			}
		}
		else
		{
			if (xCheckIdenticalMotion(pcCU, uiPartAddr))
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
			}
			else
			{
				xPredInterBi_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, pcYuvPred);
			}
		}
	}
	
}

Void TComPrediction::xPredInterUni_CNN_CNN(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, RefPicList eRefPicList, TComYuv* pcYuvPred, Bool bi)
{
	Int         iRefIdx = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);           assert(iRefIdx >= 0);
	TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(uiPartAddr);
	pcCU->clipMv(cMv);

	for (UInt comp = COMPONENT_Y; comp<pcYuvPred->getNumberValidComponents(); comp++)
	{
		const ComponentID compID = ComponentID(comp);
		xPredInterBlk_CNN_CNN(compID, pcCU, pcCU->getSlice()->getRefPic(eRefPicList, iRefIdx)->getPicYuvRec(), uiPartAddr, &cMv, iWidth, iHeight, pcYuvPred, bi, pcCU->getSlice()->getSPS()->getBitDepth(toChannelType(compID)), eRefPicList, iRefIdx);
	}
}


Void TComPrediction::xPredInterBi_CNN_CNN(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, TComYuv* pcYuvPred)
{
	TComYuv* pcMbYuv;
	Int      iRefIdx[NUM_REF_PIC_LIST_01] = { -1, -1 };

	for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
	{
		RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);
		iRefIdx[refList] = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);

		if (iRefIdx[refList] < 0)
		{
			continue;
		}

		assert(iRefIdx[refList] < pcCU->getSlice()->getNumRefIdx(eRefPicList));

		pcMbYuv = &m_acYuvPred[refList];
		if (pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(uiPartAddr) >= 0 && pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(uiPartAddr) >= 0)
		{
			xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
		}
		else
		{
			if ((pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE) ||
				(pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE))
			{
				xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
			}
			else
			{
				xPredInterUni_CNN_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv);
			}
		}
	}

	if (pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE)
	{
		xWeightedPredictionBi(pcCU, &m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred);
	}
	else if (pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE)
	{
		xWeightedPredictionUni(pcCU, &m_acYuvPred[REF_PIC_LIST_0], uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
	}
	else
	{
		xWeightedAverage(&m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred, pcCU->getSlice()->getSPS()->getBitDepths());
	}
}


Void TComPrediction::xPredInterUni_CNN_DCT(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, RefPicList eRefPicList, TComYuv* pcYuvPred, Bool bi)
{
	Int         iRefIdx = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);           assert(iRefIdx >= 0);
	TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(uiPartAddr);
	pcCU->clipMv(cMv);

	for (UInt comp = COMPONENT_Y; comp<pcYuvPred->getNumberValidComponents(); comp++)
	{
		const ComponentID compID = ComponentID(comp);
		xPredInterBlk_CNN_DCT(compID, pcCU, pcCU->getSlice()->getRefPic(eRefPicList, iRefIdx)->getPicYuvRec(), uiPartAddr, &cMv, iWidth, iHeight, pcYuvPred, bi, pcCU->getSlice()->getSPS()->getBitDepth(toChannelType(compID)), eRefPicList, iRefIdx);
	}
}


Void TComPrediction::xPredInterBi_CNN_DCT(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, TComYuv* pcYuvPred)
{
	TComYuv* pcMbYuv;
	Int      iRefIdx[NUM_REF_PIC_LIST_01] = { -1, -1 };

	for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
	{
		RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);
		iRefIdx[refList] = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);

		if (iRefIdx[refList] < 0)
		{
			continue;
		}

		assert(iRefIdx[refList] < pcCU->getSlice()->getNumRefIdx(eRefPicList));

		pcMbYuv = &m_acYuvPred[refList];
		if (pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(uiPartAddr) >= 0 && pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(uiPartAddr) >= 0)
		{
			xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
		}
		else
		{
			if ((pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE) ||
				(pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE))
			{
				xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
			}
			else
			{
				xPredInterUni_CNN_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv);
			}
		}
	}

	if (pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE)
	{
		xWeightedPredictionBi(pcCU, &m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred);
	}
	else if (pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE)
	{
		xWeightedPredictionUni(pcCU, &m_acYuvPred[REF_PIC_LIST_0], uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
	}
	else
	{
		xWeightedAverage(&m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred, pcCU->getSlice()->getSPS()->getBitDepths());
	}
}

Void TComPrediction::xPredInterUni_DCT_DCT(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, RefPicList eRefPicList, TComYuv* pcYuvPred, Bool bi)
{
	Int         iRefIdx = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);           assert(iRefIdx >= 0);
	TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(uiPartAddr);
	pcCU->clipMv(cMv);

	for (UInt comp = COMPONENT_Y; comp<pcYuvPred->getNumberValidComponents(); comp++)
	{
		const ComponentID compID = ComponentID(comp);
		xPredInterBlk_DCT_DCT(compID, pcCU, pcCU->getSlice()->getRefPic(eRefPicList, iRefIdx)->getPicYuvRec(), uiPartAddr, &cMv, iWidth, iHeight, pcYuvPred, bi, pcCU->getSlice()->getSPS()->getBitDepth(toChannelType(compID)));
	}
}

Void TComPrediction::xPredInterBi_DCT_DCT(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, TComYuv* pcYuvPred)
{
	TComYuv* pcMbYuv;
	Int      iRefIdx[NUM_REF_PIC_LIST_01] = { -1, -1 };

	for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
	{
		RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);
		iRefIdx[refList] = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);

		if (iRefIdx[refList] < 0)
		{
			continue;
		}

		assert(iRefIdx[refList] < pcCU->getSlice()->getNumRefIdx(eRefPicList));

		pcMbYuv = &m_acYuvPred[refList];
		if (pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(uiPartAddr) >= 0 && pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(uiPartAddr) >= 0)
		{
			xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
		}
		else
		{
			if ((pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE) ||
				(pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE))
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
			}
			else
			{
				xPredInterUni_DCT_DCT(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv);
			}
		}
	}

	if (pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE)
	{
		xWeightedPredictionBi(pcCU, &m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred);
	}
	else if (pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE)
	{
		xWeightedPredictionUni(pcCU, &m_acYuvPred[REF_PIC_LIST_0], uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
	}
	else
	{
		xWeightedAverage(&m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred, pcCU->getSlice()->getSPS()->getBitDepths());
	}
}


Void TComPrediction::xPredInterUni_DCT_CNN(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, RefPicList eRefPicList, TComYuv* pcYuvPred, Bool bi)
{
	Int         iRefIdx = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);           assert(iRefIdx >= 0);
	TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(uiPartAddr);
	pcCU->clipMv(cMv);

	for (UInt comp = COMPONENT_Y; comp<pcYuvPred->getNumberValidComponents(); comp++)
	{
		const ComponentID compID = ComponentID(comp);
		xPredInterBlk_DCT_CNN(compID, pcCU, pcCU->getSlice()->getRefPic(eRefPicList, iRefIdx)->getPicYuvRec(), uiPartAddr, &cMv, iWidth, iHeight, pcYuvPred, bi, pcCU->getSlice()->getSPS()->getBitDepth(toChannelType(compID)), eRefPicList, iRefIdx);
	}
}

Void TComPrediction::xPredInterBi_DCT_CNN(TComDataCU* pcCU, UInt uiPartAddr, Int iWidth, Int iHeight, TComYuv* pcYuvPred)
{
	TComYuv* pcMbYuv;
	Int      iRefIdx[NUM_REF_PIC_LIST_01] = { -1, -1 };

	for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
	{
		RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);
		iRefIdx[refList] = pcCU->getCUMvField(eRefPicList)->getRefIdx(uiPartAddr);

		if (iRefIdx[refList] < 0)
		{
			continue;
		}

		assert(iRefIdx[refList] < pcCU->getSlice()->getNumRefIdx(eRefPicList));

		pcMbYuv = &m_acYuvPred[refList];
		if (pcCU->getCUMvField(REF_PIC_LIST_0)->getRefIdx(uiPartAddr) >= 0 && pcCU->getCUMvField(REF_PIC_LIST_1)->getRefIdx(uiPartAddr) >= 0)
		{
			xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
		}
		else
		{
			if ((pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE) ||
				(pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE))
			{
				xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv, true);
			}
			else
			{
				xPredInterUni_DCT_CNN(pcCU, uiPartAddr, iWidth, iHeight, eRefPicList, pcMbYuv);
			}
		}
	}

	if (pcCU->getSlice()->getPPS()->getWPBiPred() && pcCU->getSlice()->getSliceType() == B_SLICE)
	{
		xWeightedPredictionBi(pcCU, &m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred);
	}
	else if (pcCU->getSlice()->getPPS()->getUseWP() && pcCU->getSlice()->getSliceType() == P_SLICE)
	{
		xWeightedPredictionUni(pcCU, &m_acYuvPred[REF_PIC_LIST_0], uiPartAddr, iWidth, iHeight, REF_PIC_LIST_0, pcYuvPred);
	}
	else
	{
		xWeightedAverage(&m_acYuvPred[REF_PIC_LIST_0], &m_acYuvPred[REF_PIC_LIST_1], iRefIdx[REF_PIC_LIST_0], iRefIdx[REF_PIC_LIST_1], uiPartAddr, iWidth, iHeight, pcYuvPred, pcCU->getSlice()->getSPS()->getBitDepths());
	}
}


/**
* \brief Generate motion-compensated block
*
* \param compID     Colour component ID
* \param cu         Pointer to current CU
* \param refPic     Pointer to reference picture
* \param partAddr   Address of block within CU
* \param mv         Motion vector
* \param width      Width of block
* \param height     Height of block
* \param dstPic     Pointer to destination picture
* \param bi         Flag indicating whether bipred is used
* \param  bitDepth  Bit depth
*/
//chi dich chuyen ref toi vi tri cua current block
Void TComPrediction::xPredInterBlk_DCT_DCT(const ComponentID compID, TComDataCU *cu, TComPicYuv *refPic, UInt partAddr, TComMv *mv, Int width, Int height, TComYuv *dstPic, Bool bi, const Int bitDepth)
{
	Int     refStride = refPic->getStride(compID);
	Int     dstStride = dstPic->getStride(compID);
	Int shiftHor = (2 + refPic->getComponentScaleX(compID));
	Int shiftVer = (2 + refPic->getComponentScaleY(compID));

	Int     refOffset = (mv->getHor() >> shiftHor) + (mv->getVer() >> shiftVer) * refStride;

	Pel*    ref = refPic->getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + refOffset;

	Pel*    dst = dstPic->getAddr(compID, partAddr);

	Int     xFrac = mv->getHor() & ((1 << shiftHor) - 1);
	Int     yFrac = mv->getVer() & ((1 << shiftVer) - 1);
	UInt    cxWidth = width >> refPic->getComponentScaleX(compID);
	UInt    cxHeight = height >> refPic->getComponentScaleY(compID);
	vector<int> a, b, aref;
	//cout << assumeCNN;
	const ChromaFormat chFmt = cu->getPic()->getChromaFormat();
	
	if (yFrac == 0)//nếu không di chuyển theo chiều y thì interpolate theo chiều ngang
	{
		m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
	}
	else if (xFrac == 0)//nếu không di chuyển theo chiều x thì interpolate theo chiều dọc
	{
		m_if.filterVer(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, yFrac, true, !bi, chFmt, bitDepth);
	}
	else//interpolate theo cả hai chiều
	{
		Int   tmpStride = m_filteredBlockTmp[0].getStride(compID);
		Pel*  tmp = m_filteredBlockTmp[0].getAddr(compID);

		const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;

		m_if.filterHor(compID, ref - ((vFilterSize >> 1) - 1)*refStride, refStride, tmp, tmpStride, cxWidth,
			cxHeight + vFilterSize - 1, xFrac, false, chFmt, bitDepth);
		m_if.filterVer(compID, tmp + ((vFilterSize >> 1) - 1)*tmpStride, tmpStride, dst, dstStride, cxWidth,
			cxHeight, yFrac, false, !bi, chFmt, bitDepth);

	}
}

void TComPrediction::WholeFrameInterp(const ComponentID compID, Pel * ReconFrame, int recFrameStride, int orWholeFrameHeight, int orWholeFrameWidth, int bitDepth, string SaveName)
{ 
    if(compID == COMPONENT_Y)
    {
       // cout<<"interpolate "<<SaveName<<endl;
        if(m_pcComCfg->getDecodingRefreshType() == 1)
        {
            //cout<<"change"<<endl;
            if(write_DCTIF_interp == 0)      write_DCTIF_interp = 8;
            else if(write_DCTIF_interp == 8) write_DCTIF_interp = 4;
            else if(write_DCTIF_interp == 4) write_DCTIF_interp = 2;
            else if(write_DCTIF_interp == 2) write_DCTIF_interp = 6;
            else if(write_DCTIF_interp == 1) write_DCTIF_interp = 3;
            else if(write_DCTIF_interp == 3) write_DCTIF_interp = 6;
            else if(write_DCTIF_interp == 6) write_DCTIF_interp = 10;
            else if(write_DCTIF_interp == 5) write_DCTIF_interp = 7;
            else if(write_DCTIF_interp == 7) write_DCTIF_interp = 10;
            else if(write_DCTIF_interp == 10) write_DCTIF_interp = 9;
            
        }
        else write_DCTIF_interp++;
    
        const ChromaFormat chFmt = m_filteredBlock[0][0].getChromaFormat();

        bool bi = false;
        
        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
        
        TComYuv m_filteredBlock_local[subSAMPLEs][subSAMPLEs];
        TComYuv m_filteredBlockTmp_local[subSAMPLEs];
        
        for (UInt i = 0; i < subSAMPLEs; i++)
        {
            m_filteredBlockTmp_local[i].create(orWholeFrameWidth+16, orWholeFrameHeight + 8, chFmt);
            for (UInt j = 0; j < subSAMPLEs; j++)
            {
                m_filteredBlock_local[i][j].create(orWholeFrameWidth+16, orWholeFrameHeight+1, chFmt);
            }
        }
        
        Int dstStride = m_filteredBlock_local[0][0].getStride(compID);
        Pel *dstPtr;
        string COMPONENTPATH = isLuma(compID)? "Luma" : "Chroma";
        if(compID!=COMPONENT_Y)
        {
            orWholeFrameWidth/=2;
            orWholeFrameHeight/=2;
            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
        }
        cout<<subSAMPLEs<<" : "<<orWholeFrameHeight<<"x"<<orWholeFrameWidth<<" stride: "<<dstStride<<endl;

        string command = "";
        for (int i = 0; i < subSAMPLEs; i++)
        {
            for (int j = 0; j < subSAMPLEs; j++)
            {
                int posy = i%4, posx = j%4;
                dstPtr = m_filteredBlock_local[posy][posx].getAddr(compID);
                
               // if(i*subSAMPLEs+j== 0) continue;
                
                if (i == 0)//filter row 0
                {             
                    m_if.filterHor(compID, ReconFrame, recFrameStride, dstPtr, dstStride, orWholeFrameWidth, orWholeFrameHeight, j, !bi, chFmt, bitDepth);
                    //if(j == 0) continue;
                    
                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<<  SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str());
                    for(int iindex = 0; iindex <orWholeFrameHeight; iindex++)
                    {
                        for(int jindex = 0; jindex < orWholeFrameWidth; jindex++)
                        {
                             myfile2<<dstPtr[jindex]<<"\t";
                        } 
                        myfile2<<endl;
                        dstPtr += dstStride;
                    }
                    myfile2.close();
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                        cout<<"Error in Python prediction"<<endl;

                }
                else if (j == 0)//filter col 0
                {
                    m_if.filterVer(compID, ReconFrame, recFrameStride, dstPtr, dstStride, orWholeFrameWidth, orWholeFrameHeight, i, true, !bi, chFmt, bitDepth);
                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<< SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str());
                    for(int iindex = 0; iindex <orWholeFrameHeight; iindex++)
                    {
                        for(int jindex = 0; jindex < orWholeFrameWidth; jindex++)
                        {
                             myfile2<<dstPtr[jindex]<<"\t";
                        } 
                        dstPtr += dstStride; 
                        myfile2<<endl;
                    }
                    myfile2.close();
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                       cout<<"Error in Python prediction"<<endl;
                }
                else // filter P(i,j) i,j belong to [1, 3]
                {
                    Pel *intPtr = m_filteredBlockTmp_local[posx].getAddr(compID);
                    int intStride = m_filteredBlockTmp_local[posx].getStride(compID);
                    const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;
                    
                    m_if.filterHor(compID, ReconFrame - ((vFilterSize>>1) -1)*recFrameStride, recFrameStride, intPtr, intStride, orWholeFrameWidth, 
                    orWholeFrameHeight+vFilterSize-1, j, false, chFmt, bitDepth);
                     
                    m_if.filterVer(compID, intPtr + ((vFilterSize>>1) -1)*intStride, intStride, dstPtr, dstStride, orWholeFrameWidth,
                    orWholeFrameHeight, i, false, !bi, chFmt, bitDepth);

                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<<SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str()); 
                    if(myfile2.is_open())
                    {
                        for(int iindex = 0; iindex <orWholeFrameHeight; iindex++)
                        {
                            for(int jindex = 0; jindex < orWholeFrameWidth; jindex++)
                            {
                                 myfile2<<dstPtr[jindex]<<"\t";
                            } 
                            dstPtr += dstStride;
                            myfile2<<endl; 
                        }
                    }
                    myfile2.close(); 
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                        cout<<"Error in Python prediction"<<endl; 
                }
                    
            } 
        }
        for(int i = 0; i < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; i++)
        {
            m_filteredBlockTmp_local[i].destroy();
            for(int j = 0; j < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; j++)
            {
                m_filteredBlock_local[i][j].destroy();
            }
        }        
    }
    else
    {
        const ChromaFormat chFmt = m_filteredBlock[0][0].getChromaFormat();

        bool bi = false;
        
        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
        
        TComYuv m_filteredBlock_local[subSAMPLEs][subSAMPLEs];
        TComYuv m_filteredBlockTmp_local[subSAMPLEs];
        
        for (UInt i = 0; i < subSAMPLEs; i++)
        {
            m_filteredBlockTmp_local[i].create(orWholeFrameWidth+16+2, orWholeFrameHeight + 8+2, chFmt);
            for (UInt j = 0; j < subSAMPLEs; j++)
            {
                m_filteredBlock_local[i][j].create(orWholeFrameWidth+16+2, orWholeFrameHeight+1+2, chFmt);
            }
        }
        
        Int dstStride = m_filteredBlock_local[0][0].getStride(compID);
        Pel *dstPtr;
        string COMPONENTPATH = isLuma(compID)? "Luma" : "Chroma";
        if(compID!=COMPONENT_Y)
        {
            orWholeFrameWidth/=2;
            orWholeFrameHeight/=2;
            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
        }
        //cout<<subSAMPLEs<<" : "<<orWholeFrameHeight<<"x"<<orWholeFrameWidth<<" stride: "<<dstStride<<endl;

        string command = "";
        for (int i = 0; i < subSAMPLEs; i++)
        {
            for (int j = 0; j < subSAMPLEs; j++)
            {
                int posy = i%4, posx = j%4;
                dstPtr = m_filteredBlock_local[posy][posx].getAddr(compID);
                //cout<<i<<","<<j<<endl;
                if(i*subSAMPLEs+j== 0) continue;
                
                if (i == 0)//filter row 0
                {             
                    m_if.filterHor(compID, ReconFrame-1, recFrameStride, dstPtr, dstStride, orWholeFrameWidth+1, orWholeFrameHeight, j, !bi, chFmt, bitDepth);
                    //if(j == 0) continue;
                    
                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<<  SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str());
                    for(int iindex = 0; iindex <orWholeFrameHeight; iindex++)
                    {
                        for(int jindex = 0; jindex < orWholeFrameWidth+1; jindex++)
                        {
                             myfile2<<dstPtr[jindex]<<"\t";
                        } 
                        myfile2<<endl;
                        dstPtr += dstStride;
                    }
                    myfile2.close();
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                        cout<<"Error in Python prediction"<<endl;

                }
                else if (j == 0)//filter col 0
                {
                    m_if.filterVer(compID, ReconFrame-recFrameStride, recFrameStride, dstPtr, dstStride, orWholeFrameWidth, orWholeFrameHeight+1, i, true, !bi, chFmt, bitDepth);
                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<< SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str());
                    for(int iindex = 0; iindex <orWholeFrameHeight+1; iindex++)
                    {
                        for(int jindex = 0; jindex < orWholeFrameWidth; jindex++)
                        {
                             myfile2<<dstPtr[jindex]<<"\t";
                        } 
                        dstPtr += dstStride; 
                        myfile2<<endl;
                    }
                    myfile2.close();
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                       cout<<"Error in Python prediction"<<endl;
                }
                else // filter P(i,j) i,j belong to [1, 3]
                {
                    Pel *intPtr = m_filteredBlockTmp_local[posx].getAddr(compID);
                    int intStride = m_filteredBlockTmp_local[posx].getStride(compID);
                    const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;
                    
                    m_if.filterHor(compID, ReconFrame -recFrameStride -1 - ((vFilterSize>>1) -1)*recFrameStride, recFrameStride, intPtr, intStride, orWholeFrameWidth+1, orWholeFrameHeight+vFilterSize-1+1, j, false, chFmt, bitDepth);
                     
                    m_if.filterVer(compID, intPtr + ((vFilterSize>>1) -1)*intStride, intStride, dstPtr, dstStride, orWholeFrameWidth+1,	orWholeFrameHeight+1, i, false, !bi, chFmt, bitDepth);

                    string savelinkdata = "/home/chipdk/testHM/icnn_cu_selection/build/CNNinput/";
                    stringstream ss,sscommand;
                    ss << savelinkdata <<COMPONENTPATH<<"/"<<"q"<<m_pcComCfg->getQP()<<"/"<<SaveName<<"output"<<i*subSAMPLEs+j << ".txt";
                    string savefile = ss.str();
                    ofstream myfile2(savefile.c_str()); 
                    if(myfile2.is_open())
                    {

                        for(int iindex = 0; iindex <orWholeFrameHeight+1; iindex++)
                        {
                            for(int jindex = 0; jindex < orWholeFrameWidth+1; jindex++)
                            {
                                 myfile2<<dstPtr[jindex]<<"\t";
                            } 
                            dstPtr += dstStride;
                            myfile2<<endl; 
                        }
                    }
                    myfile2.close(); 
                    sscommand<<"python /home/chipdk/15VDSRs/pytorch_VDSR/demo15in1.py " <<" --image "<<savefile;
                    command = sscommand.str();
                    //if(compID == COMPONENT_Y)
                    if(system(command.c_str()))
                        cout<<"Error in Python prediction"<<endl; 
                }
                
            }
        }  
       // cout<<"finish"<<endl;
        for(int i = 0; i < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; i++)
        {
            m_filteredBlockTmp_local[i].destroy();
            for(int j = 0; j < LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; j++)
            {
                m_filteredBlock_local[i][j].destroy();
            }
        }
        
    }
}

Int PositionofRefFrameInRefList(int ref_frameindex, Int reflist[5])
{
    for(int i = 0; i< 5;i++)
    {
        if(reflist[i] == ref_frameindex)
            return i;//return position of ref_frameindex in list reflist
    }
    return -1;
}

Void TComPrediction::xPredInterBlk_CNN_DCT(const ComponentID compID, TComDataCU *cu, TComPicYuv *refPic, UInt partAddr, TComMv *mv, Int width, Int height, TComYuv *dstPic, Bool bi, const Int bitDepth, RefPicList eRefPicList, int iRefIdxPred)
{
	Int     refStride = refPic->getStride(compID);
	Int     dstStride = dstPic->getStride(compID);
	Int shiftHor = (2 + refPic->getComponentScaleX(compID));
	Int shiftVer = (2 + refPic->getComponentScaleY(compID));
	Int     refOffset = (mv->getHor() >> shiftHor) + (mv->getVer() >> shiftVer) * refStride;
	Pel*    ref = refPic->getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + refOffset;

	Pel*    dst = dstPic->getAddr(compID, partAddr);

	Int     xFrac = mv->getHor() & ((1 << shiftHor) - 1);
	Int     yFrac = mv->getVer() & ((1 << shiftVer) - 1);
	UInt    cxWidth = width >> refPic->getComponentScaleX(compID);
	UInt    cxHeight = height >> refPic->getComponentScaleY(compID);
	const ChromaFormat chFmt = cu->getPic()->getChromaFormat();
    int ref_frameindex = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPOC();
   
    
    if (xFrac == 0 && yFrac == 0)
    {
        m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
    }
    else
    {
		if (compID == COMPONENT_Y)
		{
			int input_frame_width = cu->getPic()->getPicYuvOrg()->getWidth(COMPONENT_Y);
			int input_frame_height = cu->getPic()->getPicYuvOrg()->getHeight(COMPONENT_Y);

			Pel * recFrameY = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Y);
			Pel * recFrameCb = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb);
			Pel * recFrameCr = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr);
			int recFrameStride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Y);
			int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);

			//Pel * blk_or_ref_pel = (Pel*)malloc(sizeof(Pel*)*(input_frame_width*input_frame_height*16));//whole interpolated frame
			Pel * blk_or_ref_pel = interp_ref_pic0[0].getAddr(compID);
			int or_ref_stride = interp_ref_pic0[0].getStride(compID);
			Int reflist[4] = { -1,-1,-1,-1 };
			int numref = cu->getSlice()->getNumRefIdx(RefPicList(REF_PIC_LIST_0));
			for (int refindex = 0; refindex < numref; refindex++)
			{
				reflist[refindex] = cu->getSlice()->getRefPOC(RefPicList(REF_PIC_LIST_0), numref - refindex - 1) - cu->getSlice()->getLastIDR();
			}
			int curPOC = cu->getSlice()->getPOC();
            if(m_pcComCfg->getDecodingRefreshType() == 1)
            {
                if(curPOC == 8) {reflist[0] = 0;reflist[1] = 0;reflist[2] = 0;reflist[3] = 0;} 
                if(curPOC == 4) {reflist[0] = 0;reflist[1] = 8;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 2) {reflist[0] = 0;reflist[1] = 4;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 1) {reflist[0] = 0;reflist[1] = 2;reflist[2] = 2;reflist[3] = 4;}
                if(curPOC == 3) {reflist[0] = 2;reflist[1] = 0;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 6) {reflist[0] = 4;reflist[1] = 0;reflist[2] = 8;reflist[3] = 4;}
                if(curPOC == 5) {reflist[0] = 4;reflist[1] = 0;reflist[2] = 6;reflist[3] = 8;}
                if(curPOC == 7) {reflist[0] = 6;reflist[1] = 4;reflist[2] = 8;reflist[3] = 6;}
                if(curPOC == 10) {reflist[0] = 8;reflist[1] = 0;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 9) {reflist[0] = 8;reflist[1] = 0;reflist[2] = 10;reflist[3] = 8;}
            }
			////////////////////////////////////////////////////////////////////////
			//these line for loading reference frame to reference list 0 if it is not interpolated
			////////////////////////////////////////////////////////////////////////
			//position of ref_frameindex in list reflist
			int pos_ref_in_reflist = PositionofRefFrameInRefList(ref_frameindex, reflist);
			if (pos_ref_in_reflist == -1) cout << "reference frame out of ref list" << endl;
			string savefile = "";
			stringstream sscommandY, sscommandcb, sscommandcr;
			sscommandY << m_pcComCfg->getsequencename() << "COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcb << m_pcComCfg->getsequencename() << "COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcr << m_pcComCfg->getsequencename() << "COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;

			////////////////////////////////////////////////////////////////////////
			//these line for interpolating reference frame if it is not interpolated
			////////////////////////////////////////////////////////////////////////

			//if ref frame is interpolated in motion search, dont need to do it again (save time)
			if (write_DCTIF_interp == ref_frameindex)
			{
				cout << "compensation ref_frameindex " << ref_frameindex << endl;
				savefile = sscommandY.str();
				WholeFrameInterp(COMPONENT_Y, recFrameY, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cb component
				savefile = sscommandcb.str();
				WholeFrameInterp(COMPONENT_Cb, recFrameCb, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cr component
				savefile = sscommandcr.str();
				WholeFrameInterp(COMPONENT_Cr, recFrameCr, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                LoadReferencePic(numref, reflist);
			}

			if (encodedframe == curPOC) { LoadReferencePic(numref, reflist);}

			////////////////////////////////////////////////////////////////////////
			//these line for geting the interpolated block
			////////////////////////////////////////////////////////////////////////
			int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;

			if (ref_frameindex == reflist[0])// || pos_ref_in_reflist == 0)
			{
				or_ref_stride = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
			else if (ref_frameindex == reflist[1])//|| pos_ref_in_reflist == 1)
			{
				or_ref_stride = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}
			else if (ref_frameindex == reflist[2])//|| pos_ref_in_reflist == 2)
			{
				or_ref_stride = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}
			else //if(ref_frameindex == reflist[3])
			{
				or_ref_stride = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}
			if (blk_or_ref_pel[0] > 255 || blk_or_ref_pel[0] < 0)
			{
				cout << "error: cannot get exactly  interpolated block" << endl;
			}
			copytoMatrix(blk_or_ref_pel, or_ref_stride, dst, dstStride, cxWidth, cxHeight);
		}
		else
		{
			if (yFrac == 0)//nếu không di chuyển theo chiều y thì interpolate theo chiều ngang
			{
				m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
			}
			else if (xFrac == 0)//nếu không di chuyển theo chiều x thì interpolate theo chiều dọc
			{
				m_if.filterVer(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, yFrac, true, !bi, chFmt, bitDepth);
			}
			else//interpolate theo cả hai chiều
			{
				Int   tmpStride = m_filteredBlockTmp[0].getStride(compID);
				Pel*  tmp = m_filteredBlockTmp[0].getAddr(compID);

				const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;

				m_if.filterHor(compID, ref - ((vFilterSize >> 1) - 1)*refStride, refStride, tmp, tmpStride, cxWidth,
					cxHeight + vFilterSize - 1, xFrac, false, chFmt, bitDepth);
				m_if.filterVer(compID, tmp + ((vFilterSize >> 1) - 1)*tmpStride, tmpStride, dst, dstStride, cxWidth,
					cxHeight, yFrac, false, !bi, chFmt, bitDepth);

			}
		}
    } 
}


Void TComPrediction::xPredInterBlk_DCT_CNN(const ComponentID compID, TComDataCU *cu, TComPicYuv *refPic, UInt partAddr, TComMv *mv, Int width, Int height, TComYuv *dstPic, Bool bi, const Int bitDepth, RefPicList eRefPicList, int iRefIdxPred)
{
	Int     refStride = refPic->getStride(compID);
	Int     dstStride = dstPic->getStride(compID);
	Int shiftHor = (2 + refPic->getComponentScaleX(compID));
	Int shiftVer = (2 + refPic->getComponentScaleY(compID));
	Int     refOffset = (mv->getHor() >> shiftHor) + (mv->getVer() >> shiftVer) * refStride;
	Pel*    ref = refPic->getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + refOffset;

	Pel*    dst = dstPic->getAddr(compID, partAddr);

	Int     xFrac = mv->getHor() & ((1 << shiftHor) - 1);
	Int     yFrac = mv->getVer() & ((1 << shiftVer) - 1);
	UInt    cxWidth = width >> refPic->getComponentScaleX(compID);
	UInt    cxHeight = height >> refPic->getComponentScaleY(compID);
	const ChromaFormat chFmt = cu->getPic()->getChromaFormat();
	int ref_frameindex = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPOC();


	if (xFrac == 0 && yFrac == 0)
	{
		m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
	}
	else
	{
		if(compID == COMPONENT_Y)
		{
			if (yFrac == 0)//nếu không di chuyển theo chiều y thì interpolate theo chiều ngang
			{
				m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
			}
			else if (xFrac == 0)//nếu không di chuyển theo chiều x thì interpolate theo chiều dọc
			{
				m_if.filterVer(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, yFrac, true, !bi, chFmt, bitDepth);
			}
			else//interpolate theo cả hai chiều
			{
				Int   tmpStride = m_filteredBlockTmp[0].getStride(compID);
				Pel*  tmp = m_filteredBlockTmp[0].getAddr(compID);

				const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;

				m_if.filterHor(compID, ref - ((vFilterSize >> 1) - 1)*refStride, refStride, tmp, tmpStride, cxWidth,
					cxHeight + vFilterSize - 1, xFrac, false, chFmt, bitDepth);
				m_if.filterVer(compID, tmp + ((vFilterSize >> 1) - 1)*tmpStride, tmpStride, dst, dstStride, cxWidth,
					cxHeight, yFrac, false, !bi, chFmt, bitDepth);

			}
		}
		else
		{
			int input_frame_width = cu->getPic()->getPicYuvOrg()->getWidth(COMPONENT_Y);
			int input_frame_height = cu->getPic()->getPicYuvOrg()->getHeight(COMPONENT_Y);

			Pel * recFrameY = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Y);
			Pel * recFrameCb = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb);
			Pel * recFrameCr = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr);
			int recFrameStride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Y);
			int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);

			//Pel * blk_or_ref_pel = (Pel*)malloc(sizeof(Pel*)*(input_frame_width*input_frame_height*16));//whole interpolated frame
			Pel * blk_or_ref_pel = interp_ref_pic0[0].getAddr(compID);
			int or_ref_stride = interp_ref_pic0[0].getStride(compID);
			Int reflist[4] = { -1,-1,-1,-1 };
			int numref = cu->getSlice()->getNumRefIdx(RefPicList(REF_PIC_LIST_0));
			for (int refindex = 0; refindex < numref; refindex++)
			{
				reflist[refindex] = cu->getSlice()->getRefPOC(RefPicList(REF_PIC_LIST_0), numref - refindex - 1) - cu->getSlice()->getLastIDR();
			}
			int curPOC = cu->getSlice()->getPOC();
            if(m_pcComCfg->getDecodingRefreshType() == 1)
            {
                if(curPOC == 8) {reflist[0] = 0;reflist[1] = 0;reflist[2] = 0;reflist[3] = 0;}
                if(curPOC == 4) {reflist[0] = 0;reflist[1] = 8;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 2) {reflist[0] = 0;reflist[1] = 4;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 1) {reflist[0] = 0;reflist[1] = 2;reflist[2] = 2;reflist[3] = 4;}
                if(curPOC == 3) {reflist[0] = 2;reflist[1] = 0;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 6) {reflist[0] = 4;reflist[1] = 0;reflist[2] = 8;reflist[3] = 4;}
                if(curPOC == 5) {reflist[0] = 4;reflist[1] = 0;reflist[2] = 6;reflist[3] = 8;}
                if(curPOC == 7) {reflist[0] = 6;reflist[1] = 4;reflist[2] = 8;reflist[3] = 6;}
                if(curPOC == 10) {reflist[0] = 8;reflist[1] = 0;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 9) {reflist[0] = 8;reflist[1] = 0;reflist[2] = 10;reflist[3] = 8;}
            }
			////////////////////////////////////////////////////////////////////////
			//these line for loading reference frame to reference list 0 if it is not interpolated
			////////////////////////////////////////////////////////////////////////
			//position of ref_frameindex in list reflist
			int pos_ref_in_reflist = PositionofRefFrameInRefList(ref_frameindex, reflist);
			if (pos_ref_in_reflist == -1) cout << "reference frame out of ref list" << endl;
			string savefile = "";
			stringstream sscommandY, sscommandcb, sscommandcr;
			sscommandY << m_pcComCfg->getsequencename() << "COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcb << m_pcComCfg->getsequencename() << "COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcr << m_pcComCfg->getsequencename() << "COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;

			////////////////////////////////////////////////////////////////////////
			//these line for interpolating reference frame if it is not interpolated
			////////////////////////////////////////////////////////////////////////

			//if ref frame is interpolated in motion search, dont need to do it again (save time)
			if (write_DCTIF_interp == ref_frameindex)
			{
				cout << "compensation ref_frameindex " << ref_frameindex << endl;
				savefile = sscommandY.str();
				WholeFrameInterp(COMPONENT_Y, recFrameY, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cb component
				savefile = sscommandcb.str();
				WholeFrameInterp(COMPONENT_Cb, recFrameCb, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cr component
				savefile = sscommandcr.str();
				WholeFrameInterp(COMPONENT_Cr, recFrameCr, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                LoadReferencePic(numref, reflist);
			}

			if (encodedframe == curPOC) { LoadReferencePic(numref, reflist);}

			////////////////////////////////////////////////////////////////////////
			//these line for geting the interpolated block
			////////////////////////////////////////////////////////////////////////
			int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;

			if (ref_frameindex == reflist[0])// || pos_ref_in_reflist == 0)
			{
				or_ref_stride = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
			else if (ref_frameindex == reflist[1])//|| pos_ref_in_reflist == 1)
			{
				or_ref_stride = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}
			else if (ref_frameindex == reflist[2])//|| pos_ref_in_reflist == 2)
			{
				or_ref_stride = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}
			else //if(ref_frameindex == reflist[3])
			{
				or_ref_stride = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
				blk_or_ref_pel = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
			}

			if (blk_or_ref_pel[0] > 255 || blk_or_ref_pel[0] < 0)
			{
				cout << "error: cannot get exactly  interpolated block" << endl;
			}
			copytoMatrix(blk_or_ref_pel, or_ref_stride, dst, dstStride, cxWidth, cxHeight);
		}
	}
}

Void TComPrediction::xPredInterBlk_CNN_CNN(const ComponentID compID, TComDataCU *cu, TComPicYuv *refPic, UInt partAddr, TComMv *mv, Int width, Int height, TComYuv *dstPic, Bool bi, const Int bitDepth, RefPicList eRefPicList, int iRefIdxPred)
{
	Int     refStride = refPic->getStride(compID);
	Int     dstStride = dstPic->getStride(compID);
	Int shiftHor = (2 + refPic->getComponentScaleX(compID));
	Int shiftVer = (2 + refPic->getComponentScaleY(compID));
	Int     refOffset = (mv->getHor() >> shiftHor) + (mv->getVer() >> shiftVer) * refStride;
	Pel*    ref = refPic->getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + refOffset;

	Pel*    dst = dstPic->getAddr(compID, partAddr);

	Int     xFrac = mv->getHor() & ((1 << shiftHor) - 1);
	Int     yFrac = mv->getVer() & ((1 << shiftVer) - 1);
	UInt    cxWidth = width >> refPic->getComponentScaleX(compID);
	UInt    cxHeight = height >> refPic->getComponentScaleY(compID);
	const ChromaFormat chFmt = cu->getPic()->getChromaFormat();
	int ref_frameindex = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPOC();


	if (xFrac == 0 && yFrac == 0)
	{
		m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
	}
	else
	{
		int input_frame_width = cu->getPic()->getPicYuvOrg()->getWidth(COMPONENT_Y);
        int input_frame_height = cu->getPic()->getPicYuvOrg()->getHeight(COMPONENT_Y);

        Pel * recFrameY = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Y);
        Pel * recFrameCb = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb);
        Pel * recFrameCr = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr);
        int recFrameStride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Y);
        int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);

        //Pel * blk_or_ref_pel = (Pel*)malloc(sizeof(Pel*)*(input_frame_width*input_frame_height*16));//whole interpolated frame
        Pel * blk_or_ref_pel = interp_ref_pic0[0].getAddr(compID);
        int or_ref_stride = interp_ref_pic0[0].getStride(compID);
        Int reflist[4] = { -1,-1,-1,-1 };
        int numref = cu->getSlice()->getNumRefIdx(RefPicList(REF_PIC_LIST_0));
        for (int refindex = 0; refindex < numref; refindex++)
        {
            reflist[refindex] = cu->getSlice()->getRefPOC(RefPicList(REF_PIC_LIST_0), numref - refindex - 1) - cu->getSlice()->getLastIDR();
        }
        int curPOC = cu->getSlice()->getPOC();
            if(m_pcComCfg->getDecodingRefreshType() == 1)
            {
                if(curPOC == 8)  {reflist[0] = 0;reflist[1] = 0;reflist[2] = 0;reflist[3] = 0;}
                if(curPOC == 4)  {reflist[0] = 0;reflist[1] = 8;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 2)  {reflist[0] = 0;reflist[1] = 4;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 1)  {reflist[0] = 0;reflist[1] = 2;reflist[2] = 2;reflist[3] = 4;}
                if(curPOC == 3)  {reflist[0] = 2;reflist[1] = 0;reflist[2] = 4;reflist[3] = 8;}
                if(curPOC == 6)  {reflist[0] = 4;reflist[1] = 0;reflist[2] = 8;reflist[3] = 4;}
                if(curPOC == 5)  {reflist[0] = 4;reflist[1] = 0;reflist[2] = 6;reflist[3] = 8;}
                if(curPOC == 7)  {reflist[0] = 6;reflist[1] = 4;reflist[2] = 8;reflist[3] = 6;}
                if(curPOC == 10) {reflist[0] = 8;reflist[1] = 0;reflist[2] = 8;reflist[3] = 0;}
                if(curPOC == 9)  {reflist[0] = 8;reflist[1] = 0;reflist[2] = 10;reflist[3] = 8;}
                numref = 4;
            }
			////////////////////////////////////////////////////////////////////////
			//these line for loading reference frame to reference list 0 if it is not interpolated
			////////////////////////////////////////////////////////////////////////
			//position of ref_frameindex in list reflist
			int pos_ref_in_reflist = PositionofRefFrameInRefList(ref_frameindex, reflist);
			if (pos_ref_in_reflist == -1) cout << "reference frame out of ref list" << endl;
			string savefile = "";
			stringstream sscommandY, sscommandcb, sscommandcr;
			sscommandY << m_pcComCfg->getsequencename() << "COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcb << m_pcComCfg->getsequencename() << "COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;
			sscommandcr << m_pcComCfg->getsequencename() << "COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_" << ref_frameindex;

			////////////////////////////////////////////////////////////////////////
			//these line for interpolating reference frame if it is not interpolated
			////////////////////////////////////////////////////////////////////////

			//if ref frame is interpolated in motion search, dont need to do it again (save time)
			if (write_DCTIF_interp == ref_frameindex)
			{
				cout << "compensation ref_frameindex " << ref_frameindex << endl;
				savefile = sscommandY.str();
				WholeFrameInterp(COMPONENT_Y, recFrameY, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cb component
				savefile = sscommandcb.str();
				WholeFrameInterp(COMPONENT_Cb, recFrameCb, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);

				//do interpolation for cr component
				savefile = sscommandcr.str();
				WholeFrameInterp(COMPONENT_Cr, recFrameCr, chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                LoadReferencePic(numref, reflist);
			}

			if (encodedframe == curPOC) { LoadReferencePic(numref, reflist);}

		////////////////////////////////////////////////////////////////////////
		//these line for geting the interpolated block
		////////////////////////////////////////////////////////////////////////
		int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
	
		if (ref_frameindex == reflist[0])// || pos_ref_in_reflist == 0)
		{
			or_ref_stride = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getStride(compID);
			Int localshiftHor = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
			Int localshiftVer = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
			Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
			blk_or_ref_pel = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

		}
		else if (ref_frameindex == reflist[1])//|| pos_ref_in_reflist == 1)
		{
			or_ref_stride = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getStride(compID);
			Int localshiftHor = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
			Int localshiftVer = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
			Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
			blk_or_ref_pel = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
		}
		else if (ref_frameindex == reflist[2])//|| pos_ref_in_reflist == 2)
		{
			or_ref_stride = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getStride(compID);
			Int localshiftHor = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
			Int localshiftVer = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
			Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
			blk_or_ref_pel = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
		}
		else //if(ref_frameindex == reflist[3])
		{
			or_ref_stride = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getStride(compID);
			Int localshiftHor = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
			Int localshiftVer = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
			Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;
			blk_or_ref_pel = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;
		}

		if (blk_or_ref_pel[0] > 255 || blk_or_ref_pel[0] < 0)
		{
			cout << "error: cannot get exactly  interpolated block" << endl;
		}
		copytoMatrix(blk_or_ref_pel, or_ref_stride, dst, dstStride, cxWidth, cxHeight);

	}
}

Void TComPrediction::LoadReferencePic(int numref, int reflist[4])
{
    int NumOfValidComps = 3;
    string savefile = "";
    cout<<"LoadReferencePic "<<endl;//<<reflist[0] <<" to refpic 0"<<endl;
    stringstream sscommandY0,sscommandcb0,sscommandcr0;
    sscommandY0<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<reflist[0];
    sscommandcb0<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<reflist[0];
    sscommandcr0<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<reflist[0];
    for (UInt comp=COMPONENT_Y; comp<NumOfValidComps; comp++)
    {            
        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
        if (comp != COMPONENT_Y)
        {
            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;//break;            
        }
        for (int i = 0; i < subSAMPLEs; i++)
        {
            for (int j = 0; j < subSAMPLEs; j++)
            {
                const ComponentID cID=ComponentID(comp);
                //cout<<"i = "<<i<<" j= "<<j<<endl;
                if(i == 0 && j == 0)continue;
                stringstream ssoutputlink;
                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                
                stringstream sscommand2;
                if(comp == COMPONENT_Y)
                    savefile = sscommandY0.str();
                else if(comp == COMPONENT_Cb)
                    savefile = sscommandcb0.str();
                else
                    savefile = sscommandcr0.str();
                

                ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
                string outputpath = ssoutputlink.str();
                Pel *tmp = interp_ref_pic0[i*subSAMPLEs + j].getAddr(cID);
                int hei = interp_ref_pic0[i*subSAMPLEs + j].getHeight(cID);
                int wid = interp_ref_pic0[i*subSAMPLEs + j].getWidth(cID);
                int tmpStride = interp_ref_pic0[i*subSAMPLEs + j].getStride(cID);
                int marginX = interp_ref_pic0[i*subSAMPLEs + j].getMarginX(cID);
                int marginY = interp_ref_pic0[i*subSAMPLEs + j].getMarginY(cID);
                int deltaX = 0, deltaY = 0;
                if(comp != COMPONENT_Y)
                {
                    if(i == 0)
                    {
                        deltaX = 1;
                    }
                    else if(j == 0)
                    {
                        deltaY = 1;
                    }
                    else
                    {
                        deltaX = 1;
                        deltaY = 1;
                    }
                }
                Pel * output = readoutputfile(outputpath.c_str(), hei+deltaY, wid+deltaX);
                int outputStride = wid+deltaX;
                getdata_addmargin(comp,output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
                free(output);
            }
        }
        pic0_atv=reflist[0];
    }
    if(numref >1)
    {
        //cout<<"load "<<reflist[1] <<" to refpic 1"<<endl;
        stringstream sscommandY1,sscommandcb1,sscommandcr1;
        sscommandY1<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<reflist[1];
        sscommandcb1<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<reflist[1];
        sscommandcr1<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<reflist[1];

        for (UInt comp=COMPONENT_Y; comp<NumOfValidComps; comp++)
        {
            int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
            if (comp != COMPONENT_Y)
            {
                subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;//break;
            }
            for (int i = 0; i < subSAMPLEs; i++)
            {
                for (int j = 0; j < subSAMPLEs; j++)
                {
                    const ComponentID cID=ComponentID(comp);
                    
                    if(i == 0 && j == 0)continue;
                    stringstream ssoutputlink;
                    string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                    
                    stringstream sscommand2;
                    if(comp == COMPONENT_Y)
                        savefile = sscommandY1.str();
                    else if(comp == COMPONENT_Cb)
                        savefile = sscommandcb1.str();
                    else
                        savefile = sscommandcr1.str();
                    

                    ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
                    string outputpath = ssoutputlink.str();
                    Pel *tmp = interp_ref_pic1[i*subSAMPLEs + j].getAddr(cID);
                    int hei = interp_ref_pic1[i*subSAMPLEs + j].getHeight(cID);
                    int wid = interp_ref_pic1[i*subSAMPLEs + j].getWidth(cID);
                    int tmpStride = interp_ref_pic1[i*subSAMPLEs + j].getStride(cID);
                    int marginX = interp_ref_pic1[i*subSAMPLEs + j].getMarginX(cID);
                    int marginY = interp_ref_pic1[i*subSAMPLEs + j].getMarginY(cID);
                    
                    int deltaX = 0, deltaY = 0;
                    if(comp != COMPONENT_Y)
                    {
                        if(i == 0)
                        {
                            deltaX = 1;
                        }
                        else if(j == 0)
                        {
                            deltaY = 1;
                        }
                        else
                        {
                            deltaX = 1;
                            deltaY = 1;
                        }
                    }
                    Pel * output = readoutputfile(outputpath.c_str(), hei+deltaY, wid+deltaX);
                    int outputStride = wid+deltaX;
                    getdata_addmargin(comp,output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
                    free(output);
                }
            }
        }
        pic1_atv = reflist[1];
    }
    if(numref > 2)
    {
        //cout<<"load "<<reflist[2] <<" to refpic 2"<<endl;
        stringstream sscommandY2,sscommandcb2,sscommandcr2;
        sscommandY2<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<reflist[2];
        sscommandcb2<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<reflist[2];
        sscommandcr2<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<reflist[2];

        for (UInt comp=COMPONENT_Y; comp<NumOfValidComps; comp++)
        {
            int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
            if (comp != COMPONENT_Y)
            {
                subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;//break;
            }
            for (int i = 0; i < subSAMPLEs; i++)
            {
                for (int j = 0; j < subSAMPLEs; j++)
                {
                    const ComponentID cID=ComponentID(comp);
                    
                    if(i == 0 && j == 0)continue;
                    stringstream ssoutputlink;
                    string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                    
                    stringstream sscommand2;
                    if(comp == COMPONENT_Y)
                        savefile = sscommandY2.str();
                    else if(comp == COMPONENT_Cb)
                        savefile = sscommandcb2.str();
                    else
                        savefile = sscommandcr2.str();
                    

                    ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
                    string outputpath = ssoutputlink.str();
                    Pel *tmp = interp_ref_pic2[i*subSAMPLEs + j].getAddr(cID);
                    int hei = interp_ref_pic2[i*subSAMPLEs + j].getHeight(cID);
                    int wid = interp_ref_pic2[i*subSAMPLEs + j].getWidth(cID);
                    int tmpStride = interp_ref_pic2[i*subSAMPLEs + j].getStride(cID);
                    int marginX = interp_ref_pic2[i*subSAMPLEs + j].getMarginX(cID);
                    int marginY = interp_ref_pic2[i*subSAMPLEs + j].getMarginY(cID);

                    int deltaX = 0, deltaY = 0;
                    if(comp != COMPONENT_Y)
                    {
                        if(i == 0)
                        {
                            deltaX = 1;
                        }
                        else if(j == 0)
                        {
                            deltaY = 1;
                        }
                        else
                        {
                            deltaX = 1;
                            deltaY = 1;
                        }
                    }
                    Pel * output = readoutputfile(outputpath.c_str(), hei+deltaY, wid+deltaX);
                    int outputStride = wid+deltaX;
                    getdata_addmargin(comp,output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
                    free(output);
                }
            }
        }
        pic2_atv=reflist[2];
    }
    if(numref >3)
    {
        //cout<<"load "<<reflist[3] <<" to refpic 3"<<endl;
        stringstream sscommandY3,sscommandcb3,sscommandcr3;
        sscommandY3<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<reflist[3];
        sscommandcb3<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<reflist[3];
        sscommandcr3<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<reflist[3];
        
        for (UInt comp=COMPONENT_Y; comp<NumOfValidComps; comp++)
        {
            int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
            if (comp != COMPONENT_Y)
            {
                subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;//break;
            }
            for (int i = 0; i < subSAMPLEs; i++)
            {
                for (int j = 0; j < subSAMPLEs; j++)
                {
                    const ComponentID cID=ComponentID(comp);
                    
                    if(i == 0 && j == 0)continue;
                    stringstream ssoutputlink;
                    string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                    
                    stringstream sscommand2;
                    if(comp == COMPONENT_Y)
                        savefile = sscommandY3.str();
                    else if(comp == COMPONENT_Cb)
                        savefile = sscommandcb3.str();
                    else
                        savefile = sscommandcr3.str();
                    

                    ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
                    string outputpath = ssoutputlink.str();
                    Pel *tmp = interp_ref_pic3[i*subSAMPLEs + j].getAddr(cID);
                    int hei = interp_ref_pic3[i*subSAMPLEs + j].getHeight(cID);
                    int wid = interp_ref_pic3[i*subSAMPLEs + j].getWidth(cID);
                    int tmpStride = interp_ref_pic3[i*subSAMPLEs + j].getStride(cID);
                    int marginX = interp_ref_pic3[i*subSAMPLEs + j].getMarginX(cID);
                    int marginY = interp_ref_pic3[i*subSAMPLEs + j].getMarginY(cID);

                    int deltaX = 0, deltaY = 0;
                    if(comp != COMPONENT_Y)
                    {
                        if(i == 0)
                        {
                            deltaX = 1;
                        }
                        else if(j == 0)
                        {
                            deltaY = 1;
                        }
                        else
                        {
                            deltaX = 1;
                            deltaY = 1;
                        }
                    }
                    Pel * output = readoutputfile(outputpath.c_str(), hei+deltaY, wid+deltaX);
                    int outputStride = wid+deltaX;
                    getdata_addmargin(comp,output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
                    free(output);
                }
            }
        }
        pic3_atv = reflist[3];
    }
    cout<<"reflist: "<<reflist[0]<<", "<<reflist[1]<<", "<<reflist[2]<<", "<<reflist[3]<<endl;
    if(m_pcComCfg->getDecodingRefreshType() == 1)
    {
         
        if(encodedframe == 0) encodedframe = 8;
        else if(encodedframe == 8) encodedframe = 4;
        else if(encodedframe == 4) encodedframe = 2;
        else if(encodedframe == 2) encodedframe = 1;
        else if(encodedframe == 1) encodedframe = 3;
        else if(encodedframe == 3) encodedframe = 6;
        else if(encodedframe == 6) encodedframe = 5;
        else if(encodedframe == 5) encodedframe = 7;
        else if(encodedframe == 7) encodedframe = 10;
        else if(encodedframe == 10) encodedframe = 9;
        else if(encodedframe == 9) encodedframe = 11;
        //cout<<"encodedframe"<<encodedframe<<endl;
        //cout<<"write_DCTIF_interp"<<encodedframe<<endl;
    }
    else encodedframe++;
} 

/*
Void TComPrediction::xPredInterBlkCNN(const ComponentID compID, TComDataCU *cu, TComPicYuv *refPic, UInt partAddr, TComMv *mv, Int width, Int height, TComYuv *dstPic, Bool bi, const Int bitDepth, RefPicList eRefPicList, int iRefIdxPred)
{
	Int     refStride = refPic->getStride(compID);
	Int     dstStride = dstPic->getStride(compID);
	Int shiftHor = (2 + refPic->getComponentScaleX(compID));
	Int shiftVer = (2 + refPic->getComponentScaleY(compID));
	Int     refOffset = (mv->getHor() >> shiftHor) + (mv->getVer() >> shiftVer) * refStride;
	Pel*    ref = refPic->getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + refOffset;

	Pel*    dst = dstPic->getAddr(compID, partAddr);

	Int     xFrac = mv->getHor() & ((1 << shiftHor) - 1);
	Int     yFrac = mv->getVer() & ((1 << shiftVer) - 1);
	UInt    cxWidth = width >> refPic->getComponentScaleX(compID);
	UInt    cxHeight = height >> refPic->getComponentScaleY(compID);
	const ChromaFormat chFmt = cu->getPic()->getChromaFormat();

	if (compID == COMPONENT_Y)
	{ 
		if (xFrac == 0 && yFrac == 0)
		{
			m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
		}
		else
		{
			int frameindex = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPOC();
			int input_frame_width = cu->getPic()->getPicYuvOrg()->getWidth(compID);
			int input_frame_height = cu->getPic()->getPicYuvOrg()->getHeight(compID);

			Pel * recFrame = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(compID);
			int recFrameStride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(compID);
			//Pel * blk_or_ref_pel = (Pel*)malloc(sizeof(Pel*)*(input_frame_width*input_frame_height*16));//whole interpolated frame
			Pel * blk_or_ref_pel = interp_ref_pic0[0].getAddr(compID);
			int or_ref_stride = interp_ref_pic0[0].getStride(compID);

			string savefile = "";

			if (frameindex == 0)
			{
				if (-1 == pic0_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic0[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic0[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic0[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic0[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic0[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic0[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic0_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic0[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            
            if (frameindex == 1)
			{
				if (-1 == pic1_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic1[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic1[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic1[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic1[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic1[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic1[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic1_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic1[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 2)
			{
				if (-1 == pic2_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic2[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic2[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic2[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic2[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic2[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic2[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic2_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic2[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 3)
			{
				if (-1 == pic3_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic3[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic3[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic3[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic3[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic3[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic3[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic3_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic3[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 4)
			{
				if (-1 == pic4_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                   int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic4[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic4[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic4[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic4[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic4[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic4[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic4_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic4[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic4[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic4[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic4[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 5)
			{
				if (-1 == pic5_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic5[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic5[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic5[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic5[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic5[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic5[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic5_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic5[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic5[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic5[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic5[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 6)
			{
				if (-1 == pic6_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic6[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic6[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic6[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic6[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic6[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic6[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic6_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic6[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic6[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic6[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic6[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 7)
			{
				if (-1 == pic7_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic7[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic7[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic7[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic7[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic7[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic7[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic7_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic7[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic7[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic7[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic7[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 8)
			{
				if (-1 == pic8_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic8[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic8[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic8[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic8[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic8[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic8[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic8_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic8[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic8[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic8[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic8[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 9)
			{
				if (-1 == pic9_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic9[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic9[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic9[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic9[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic9[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic9[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic9_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic9[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic9[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic9[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic9[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
            if (frameindex == 10)
			{
				if (-1 == pic10_atv)//chưa dc đọc lên
				{
					stringstream sscommandY;
                    sscommandY<<m_pcComCfg->getsequencename() <<"COMPONENT_Y" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandY.str();
                    //if Y component is interpolated in motion search, dont need to do it again (save time)
                    if(write_DCTIF_interp == frameindex)
                    {
                        WholeFrameInterp(COMPONENT_Y, recFrame, recFrameStride, input_frame_height, input_frame_width, bitDepth, savefile);
                    }
                    
                    //int chromastride = cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getStride(COMPONENT_Cb);
                    
                    //do interpolation for cb component
                    stringstream sscommandcb;
                    sscommandcb<<m_pcComCfg->getsequencename() <<"COMPONENT_Cb" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcb.str();
                    //WholeFrameInterp(COMPONENT_Cb, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cb), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    //do interpolation for cr component
                    stringstream sscommandcr;
                    sscommandcr<<m_pcComCfg->getsequencename() <<"COMPONENT_Cr" << "q" << m_pcComCfg->getQP() << "_"<<frameindex;
                    savefile = sscommandcr.str();
                    //WholeFrameInterp(COMPONENT_Cr, cu->getSlice()->getRefPic(eRefPicList, iRefIdxPred)->getPicYuvRec()->getAddr(COMPONENT_Cr), chromastride, input_frame_height, input_frame_width, bitDepth, savefile);
                    
                    
                    for (UInt comp=COMPONENT_Y; comp<dstPic->getNumberValidComponents(); comp++)
                    {
                        int subSAMPLEs = LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
                        if (comp != COMPONENT_Y)
                        {
                            subSAMPLEs = CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS; break;
                        }
                        for (int i = 0; i < subSAMPLEs; i++)
                        {
                            for (int j = 0; j < subSAMPLEs; j++)
                            {
                                const ComponentID cID=ComponentID(comp);
                                
                                if(i == 0 && j == 0)continue;
                                stringstream ssoutputlink;
                                string outputfolder = "/home/chipdk/testHM/icnn_cu_selection/build/CNNoutput/";
                                
                                stringstream sscommand2;
                                if(comp == COMPONENT_Y)
                                    savefile = sscommandY.str();
                                else if(comp == COMPONENT_Cb)
                                    savefile = sscommandcb.str();
                                else
                                    savefile = sscommandcr.str();
                                

								ssoutputlink << outputfolder << savefile << "output" << i*subSAMPLEs + j << ".txt";
								string outputpath = ssoutputlink.str();
								Pel *tmp = interp_ref_pic10[i*subSAMPLEs + j].getAddr(cID);
								int hei = interp_ref_pic10[i*subSAMPLEs + j].getHeight(cID);
								int wid = interp_ref_pic10[i*subSAMPLEs + j].getWidth(cID);
								int tmpStride = interp_ref_pic10[i*subSAMPLEs + j].getStride(cID);
								int marginX = interp_ref_pic10[i*subSAMPLEs + j].getMarginX(cID);
								int marginY = interp_ref_pic10[i*subSAMPLEs + j].getMarginY(cID);

								Pel * output = readoutputfile(outputpath.c_str(), hei, wid);
								int outputStride = wid;
								getdata_addmargin(output, outputStride, hei, wid, tmp, tmpStride, marginY, marginX, i, j);
							}
						}
					}
					pic10_atv = 1;
				}
				int subSAMPLEs = isLuma(compID) ? LUMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS : CHROMA_INTERPOLATION_FILTER_SUB_SAMPLE_POSITIONS;
				or_ref_stride = interp_ref_pic10[yFrac * subSAMPLEs + xFrac].getStride(compID);
				Int localshiftHor = (2 + interp_ref_pic10[yFrac * subSAMPLEs + xFrac].getComponentScaleX(compID));
				Int localshiftVer = (2 + interp_ref_pic10[yFrac * subSAMPLEs + xFrac].getComponentScaleY(compID));
				Int localrefOffset = (mv->getHor() >> localshiftHor) + (mv->getVer() >> localshiftVer) * or_ref_stride;

				blk_or_ref_pel = interp_ref_pic10[yFrac * subSAMPLEs + xFrac].getAddr(compID, cu->getCtuRsAddr(), cu->getZorderIdxInCtu() + partAddr) + localrefOffset;

			}
			if (blk_or_ref_pel[0] > 255 || blk_or_ref_pel[0] < 0)
			{
				cout << "error: cannot get exactly  interpolated block" << endl;
			}
			copytoMatrix(blk_or_ref_pel, or_ref_stride, dst, dstStride, cxWidth, cxHeight);
		}
	}
	else {
		if (yFrac == 0)//nếu không di chuyển theo chiều y thì interpolate theo chiều ngang
		{
			m_if.filterHor(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, xFrac, !bi, chFmt, bitDepth);
		}
		else if (xFrac == 0)//nếu không di chuyển theo chiều x thì interpolate theo chiều dọc
		{
			m_if.filterVer(compID, ref, refStride, dst, dstStride, cxWidth, cxHeight, yFrac, true, !bi, chFmt, bitDepth);
		}
		else//interpolate theo cả hai chiều
		{
			Int   tmpStride = m_filteredBlockTmp[0].getStride(compID);
			Pel*  tmp = m_filteredBlockTmp[0].getAddr(compID);

			const Int vFilterSize = isLuma(compID) ? NTAPS_LUMA : NTAPS_CHROMA;

			m_if.filterHor(compID, ref - ((vFilterSize >> 1) - 1)*refStride, refStride, tmp, tmpStride, cxWidth,
				cxHeight + vFilterSize - 1, xFrac, false, chFmt, bitDepth);
			m_if.filterVer(compID, tmp + ((vFilterSize >> 1) - 1)*tmpStride, tmpStride, dst, dstStride, cxWidth,
				cxHeight, yFrac, false, !bi, chFmt, bitDepth);
		}
	}
}
*/
void TComPrediction::getdata_withmargin(Pel* input, int inputStride, int FrameHeight, int FrameWidth, int width, int height, Pel *dst, int dstStride, int marginY, int marginX, int posy, int posx)
{
	input += posy*inputStride;
	int heightindex = 0, widthindex = 0;

	int realH = height + marginX, realW = width + marginX;


	//Pel *bkdst = dst;
	dst = dst - marginY*dstStride - marginX;

	for (int i = 0; i < FrameHeight; i += 4)
	{
		widthindex = 0;

		//set left margin X
		for (int j = 0; j < marginX; j++)
		{
			dst[widthindex++] = input[posx];
		}

		for (int j = posx; j < FrameWidth; j += 4)
		{
			dst[widthindex++] = input[j];

			if (widthindex == realW)
			{//right margin X
				for (int k = 0; k < marginX; k++)
				{
					dst[widthindex++] = input[j];
				}
				break;
			}
		}
		heightindex++;

		if (heightindex == 1)//top margin
		{
			for (int k = 0; k < marginY; k++)
			{
				copytoMatrix(dst, dstStride, dst + dstStride, dstStride, realW + marginX, 1);
				dst += dstStride;
				heightindex++;
			}

		}
		if (heightindex == realH)//bottom margin
		{
			for (int k = 0; k < marginY; k++)
			{
				copytoMatrix(dst, dstStride, dst + dstStride, dstStride, realW + marginX, 1);
				dst += dstStride;
			}
		}
		if (heightindex == realH) break;
		input += 4 * inputStride;
		dst += dstStride;
	}
}

Pel * TComPrediction::readoutputfile(const char* outputpath, int height, int width)
{
	fstream CNNoutputfile;
	CNNoutputfile.open(outputpath);
	Pel * CNNoutput = (Pel*)malloc(sizeof(Pel*)*(height*width));
	if (CNNoutputfile)
	{
		//cout<<"read "<<fileindex<<endl;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				string aline;
				getline(CNNoutputfile, aline);
				CNNoutput[i*width + j] = atoi(aline.c_str());
			}
		}
		CNNoutputfile.close();
	}
	else cout << "could not open" << outputpath << endl;
	return CNNoutput;
}

void TComPrediction::getdata_addmargin( UInt comp, Pel* input, int inputStride, int FrameHeight, int FrameWidth, Pel *dst, int dstStride, int marginY, int marginX, int posy, int posx)
{
    if(comp == COMPONENT_Y)
    {
        dst = dst - marginY*dstStride - marginX;
        int heightindex = 0;
        for (int i = 0; i < FrameHeight; i++)
        {
            int widthindex = 0;

            //set left margin X
            for (int j = 0; j < marginX; j++)
            {
                dst[widthindex++] = input[0];
            }

            //real data
            for (int j = 0; j <FrameWidth; j++)
            {
                dst[widthindex++] = input[j];
            }

            //right margin X
            for (int k = 0; k < marginX; k++)
            {
                dst[widthindex++] = input[FrameWidth - 1];
            }
            heightindex++;
            //top margin
            if (heightindex == 1 || heightindex == marginY + FrameHeight - 1)
            {
                for (int k = 0; k < marginY; k++)
                {
                    copytoMatrix(dst, dstStride, dst + dstStride, dstStride, FrameWidth + 2 * marginX, 1);
                    dst += dstStride;
                    heightindex++;
                }
            }
            input += inputStride;
            dst += dstStride;
        }
    }
    else
    {
        Pel *track1 = dst, *track2 = dst;
        Pel *trackinput = input;
        int deltaY = 0, deltaX = 0;
        if(posy == 0)
        {
            deltaX = 1;
        }
        else if(posx == 0)
        {
            deltaY = 1;
        }
        else
        {
            deltaX = 1;
            deltaY = 1;
        }
        int marginL = marginX-deltaX, marginR = marginX;
        int marginT = marginY-deltaY, marginB = marginY;
        dst = dst - marginY*dstStride - marginX;
        int heightindex = 0;
        for (int i = 0; i < FrameHeight+deltaY; i++)
        {
            int widthindex = 0;

            //set left margin X
            for (int j = 0; j < marginL; j++)
            {
                dst[widthindex++] = input[0];
            }

            //real data
            for (int j = 0; j <FrameWidth+deltaX; j++)
            {
                dst[widthindex++] = input[j];
            }

            //right margin X
            for (int k = 0;  k < marginX; k++)
            {
                dst[widthindex++] = input[FrameWidth-1+deltaX]; 
            } 
            heightindex++;
            //top margin
            if (heightindex == 1)
            { 
                for (int k = 0; k < marginY-deltaY; k++)
                {
                    copytoMatrix(dst, dstStride, dst + dstStride, dstStride, FrameWidth + 2 * marginX, 1);
                    dst += dstStride;
                    heightindex++;
                }
            }
            if (heightindex == marginY+ FrameHeight)
            {
                for (int k = 0; k < marginY; k++)
                {
                    copytoMatrix(dst, dstStride, dst + dstStride, dstStride, FrameWidth + 2 * marginX, 1);
                    dst += dstStride;
                    heightindex++;
                }
            }
            input += inputStride;
            dst += dstStride;
        }
        /*
        if(posy == 7 && posx == 7 && comp == COMPONENT_Cb)
        {
            cout<<"read from output file"<< FrameHeight<<"x"<<FrameWidth<<endl;
            for(int i = 0; i< FrameHeight+deltaY;i++)
            {
                for(int j = 0; j < FrameWidth+deltaX;j++)
                {
                    cout<<trackinput[j]<<"\t";
                }
                cout<<endl;
                trackinput+=inputStride;
            }
            cout<<"end read from file"<<endl;;
            
            cout<<"read from file track1 "<< 2*marginY+FrameHeight<<"x"<<FrameWidth + 2 * marginX <<endl;
            track1 = track1- marginY*dstStride - marginX;
            for(int i = 0; i< 2*marginY+FrameHeight;i++)
            {
                for(int j = 0; j < FrameWidth + 2 * marginX;j++)
                {
                    cout<<track1[j]<<"\t";
                }
                cout<<endl;
                track1+=dstStride;
            }
            cout<<"end read from file"<<endl;;
            
            cout<<"read from file track2 "<< FrameHeight<<"x"<<FrameWidth <<endl;
            for(int i = 0; i< FrameHeight;i++)
            {
                for(int j = 0; j < FrameWidth;j++)
                {
                    cout<<track2[j]<<"\t";
                }
                cout<<endl;
                track2+=dstStride;
            }
            cout<<"end read from file";
        }
        */
    }
}

void TComPrediction::copytoMatrix(Pel * src, int srcStride, Pel * dst, int dstStride, int width, int height)
{
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			dst[j] = src[j];
		}
		dst += dstStride;
		src += srcStride;
	}
}
Void TComPrediction::xWeightedAverage(TComYuv* pcYuvSrc0, TComYuv* pcYuvSrc1, Int iRefIdx0, Int iRefIdx1, UInt uiPartIdx, Int iWidth, Int iHeight, TComYuv* pcYuvDst, const BitDepths &clipBitDepths)
{
	if (iRefIdx0 >= 0 && iRefIdx1 >= 0)
	{
		pcYuvDst->addAvg(pcYuvSrc0, pcYuvSrc1, uiPartIdx, iWidth, iHeight, clipBitDepths);
	}
	else if (iRefIdx0 >= 0 && iRefIdx1 <  0)
	{
		pcYuvSrc0->copyPartToPartYuv(pcYuvDst, uiPartIdx, iWidth, iHeight);
	}
	else if (iRefIdx0 <  0 && iRefIdx1 >= 0)
	{
		pcYuvSrc1->copyPartToPartYuv(pcYuvDst, uiPartIdx, iWidth, iHeight);
	}
}

// AMVP
Void TComPrediction::getMvPredAMVP(TComDataCU* pcCU, UInt uiPartIdx, UInt uiPartAddr, RefPicList eRefPicList, TComMv& rcMvPred)
{
	AMVPInfo* pcAMVPInfo = pcCU->getCUMvField(eRefPicList)->getAMVPInfo();

	if (pcAMVPInfo->iN <= 1)
	{
		rcMvPred = pcAMVPInfo->m_acMvCand[0];

		pcCU->setMVPIdxSubParts(0, eRefPicList, uiPartAddr, uiPartIdx, pcCU->getDepth(uiPartAddr));
		pcCU->setMVPNumSubParts(pcAMVPInfo->iN, eRefPicList, uiPartAddr, uiPartIdx, pcCU->getDepth(uiPartAddr));
		return;
	}

	assert(pcCU->getMVPIdx(eRefPicList, uiPartAddr) >= 0);
	rcMvPred = pcAMVPInfo->m_acMvCand[pcCU->getMVPIdx(eRefPicList, uiPartAddr)];
	return;
}

/** Function for deriving planar intra prediction.
* \param pSrc        pointer to reconstructed sample array
* \param srcStride   the stride of the reconstructed sample array
* \param rpDst       reference to pointer for the prediction sample array
* \param dstStride   the stride of the prediction sample array
* \param width       the width of the block
* \param height      the height of the block
* \param channelType type of pel array (luma, chroma)
* \param format      chroma format
*
* This function derives the prediction samples for planar mode (intra coding).
*/
//NOTE: Bit-Limit - 24-bit source
Void TComPrediction::xPredIntraPlanar(const Pel* pSrc, Int srcStride, Pel* rpDst, Int dstStride, UInt width, UInt height)
{
	assert(width <= height);

	Int leftColumn[MAX_CU_SIZE + 1], topRow[MAX_CU_SIZE + 1], bottomRow[MAX_CU_SIZE], rightColumn[MAX_CU_SIZE];
	UInt shift1Dhor = g_aucConvertToBit[width] + 2;
	UInt shift1Dver = g_aucConvertToBit[height] + 2;

	// Get left and above reference column and row
	for (Int k = 0; k<width + 1; k++)
	{
		topRow[k] = pSrc[k - srcStride];
	}

	for (Int k = 0; k < height + 1; k++)
	{
		leftColumn[k] = pSrc[k*srcStride - 1];
	}

	// Prepare intermediate variables used in interpolation
	Int bottomLeft = leftColumn[height];
	Int topRight = topRow[width];

	for (Int k = 0; k<width; k++)
	{
		bottomRow[k] = bottomLeft - topRow[k];
		topRow[k] <<= shift1Dver;
	}

	for (Int k = 0; k<height; k++)
	{
		rightColumn[k] = topRight - leftColumn[k];
		leftColumn[k] <<= shift1Dhor;
	}

	const UInt topRowShift = 0;

	// Generate prediction signal
	for (Int y = 0; y<height; y++)
	{
		Int horPred = leftColumn[y] + width;
		for (Int x = 0; x<width; x++)
		{
			horPred += rightColumn[y];
			topRow[x] += bottomRow[x];

			Int vertPred = ((topRow[x] + topRowShift) >> topRowShift);
			rpDst[y*dstStride + x] = (horPred + vertPred) >> (shift1Dhor + 1);
		}
	}
}

/** Function for filtering intra DC predictor.
* \param pSrc pointer to reconstructed sample array
* \param iSrcStride the stride of the reconstructed sample array
* \param pDst reference to pointer for the prediction sample array
* \param iDstStride the stride of the prediction sample array
* \param iWidth the width of the block
* \param iHeight the height of the block
* \param channelType type of pel array (luma, chroma)
*
* This function performs filtering left and top edges of the prediction samples for DC mode (intra coding).
*/
Void TComPrediction::xDCPredFiltering(const Pel* pSrc, Int iSrcStride, Pel* pDst, Int iDstStride, Int iWidth, Int iHeight, ChannelType channelType)
{
	Int x, y, iDstStride2, iSrcStride2;

	if (isLuma(channelType) && (iWidth <= MAXIMUM_INTRA_FILTERED_WIDTH) && (iHeight <= MAXIMUM_INTRA_FILTERED_HEIGHT))
	{
		//top-left
		pDst[0] = (Pel)((pSrc[-iSrcStride] + pSrc[-1] + 2 * pDst[0] + 2) >> 2);

		//top row (vertical filter)
		for (x = 1; x < iWidth; x++)
		{
			pDst[x] = (Pel)((pSrc[x - iSrcStride] + 3 * pDst[x] + 2) >> 2);
		}

		//left column (horizontal filter)
		for (y = 1, iDstStride2 = iDstStride, iSrcStride2 = iSrcStride - 1; y < iHeight; y++, iDstStride2 += iDstStride, iSrcStride2 += iSrcStride)
		{
			pDst[iDstStride2] = (Pel)((pSrc[iSrcStride2] + 3 * pDst[iDstStride2] + 2) >> 2);
		}
	}

	return;
}

/* Static member function */
Bool TComPrediction::UseDPCMForFirstPassIntraEstimation(TComTU &rTu, const UInt uiDirMode)
{
	return (rTu.getCU()->isRDPCMEnabled(rTu.GetAbsPartIdxTU())) &&
		rTu.getCU()->getCUTransquantBypass(rTu.GetAbsPartIdxTU()) &&
		(uiDirMode == HOR_IDX || uiDirMode == VER_IDX);
}

#if MCTS_ENC_CHECK

Void getRefPUPartPos(TComDataCU* pcCU, TComMv& cMv, Int uiPartIdx, Int& ruiPredXLeft, Int& ruiPredYTop, Int& ruiPredXRight, Int& ruiPredYBottom, Int iWidth, Int iHeight)
{
	ruiPredXLeft = pcCU->getCUPelX();
	ruiPredYTop = pcCU->getCUPelY();

	switch (pcCU->getPartitionSize(0))
	{
	case SIZE_2NxN:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + (iHeight << 1);
			ruiPredYTop += iHeight;
		}
		break;
	case SIZE_Nx2N:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			ruiPredXRight = ruiPredXLeft + (iWidth << 1);
			ruiPredYBottom = ruiPredYTop + iHeight;
			ruiPredXLeft += iWidth;
		}
		break;
	case SIZE_NxN:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else if (uiPartIdx == 1)
		{
			ruiPredXRight = ruiPredXLeft + (iWidth << 1);
			ruiPredYBottom = ruiPredYTop + iHeight;
			ruiPredXLeft += iWidth;
		}
		else if (uiPartIdx == 2)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + (iHeight << 1);
			ruiPredYTop += iHeight;
		}
		else if (uiPartIdx == 3)
		{
			ruiPredXRight = ruiPredXLeft + (iWidth << 1);
			ruiPredYBottom = ruiPredYTop + (iHeight << 1);
			ruiPredXLeft += iWidth;
			ruiPredYTop += iHeight;
		}
		break;
	case SIZE_2NxnU:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + pcCU->getHeight(0);
			ruiPredYTop += (iHeight / 3);
		}
		break;
	case SIZE_2NxnD:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			Int oriHeight = iHeight << 2;
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + oriHeight;
			ruiPredYTop += (oriHeight >> 2) + (oriHeight >> 1);
		}
		break;
	case SIZE_nLx2N:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			ruiPredXRight = ruiPredXLeft + pcCU->getWidth(0);
			ruiPredYBottom = ruiPredYTop + iHeight;
			ruiPredXLeft += (iWidth / 3);
		}
		break;
	case SIZE_nRx2N:
		if (uiPartIdx == 0)
		{
			ruiPredXRight = ruiPredXLeft + iWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
		}
		else
		{
			Int oriWidth = (iWidth << 2);
			ruiPredXRight = ruiPredXLeft + oriWidth;
			ruiPredYBottom = ruiPredYTop + iHeight;
			ruiPredXLeft += (oriWidth >> 2) + (oriWidth >> 1);
		}
		break;
	default:
		ruiPredXRight = ruiPredXLeft + iWidth;
		ruiPredYBottom = ruiPredYTop + iHeight;
		break;
	}

	ruiPredXLeft += (cMv.getHor() >> 2);
	ruiPredYTop += (cMv.getVer() >> 2);
	ruiPredXRight += (cMv.getHor() >> 2) - 1;
	ruiPredYBottom += (cMv.getVer() >> 2) - 1;
}

Bool checkMVPRange(TComMv& cMv, UInt ctuLength, UInt tileXPosInCtus, UInt tileYPosInCtus, UInt tileWidthtInCtus, UInt tileHeightInCtus, Int PredXLeft, Int PredXRight, Int PredYTop, Int PredYBottom, ChromaFormat chromaFormat)
{
	// filter length of sub-sample generation filter to be considered
	const UInt LumaLTSampleOffset = 3;
	const UInt LumaRBSampleOffset = 4;
	const UInt CromaLTSampleoffset = 1;
	const UInt CromaRBSampleoffset = 2;

	// tile position in full pels
	const Int leftTopPelPosX = ctuLength * tileXPosInCtus;
	const Int leftTopPelPosY = ctuLength * tileYPosInCtus;
	const Int rightBottomPelPosX = ((tileWidthtInCtus + tileXPosInCtus) * ctuLength) - 1;
	const Int rightBottomPelPosY = ((tileHeightInCtus + tileYPosInCtus) * ctuLength) - 1;

	// Luma MV range check
	const Bool isFullPelHorLuma = (cMv.getHor() % 4 == 0);
	const Bool isFullPelVerLuma = (cMv.getVer() % 4 == 0);

	const Int lRangeXLeft = leftTopPelPosX + (isFullPelHorLuma ? 0 : LumaLTSampleOffset);
	const Int lRangeYTop = leftTopPelPosY + (isFullPelVerLuma ? 0 : LumaLTSampleOffset);
	const Int lRangeXRight = rightBottomPelPosX - (isFullPelHorLuma ? 0 : LumaRBSampleOffset);
	const Int lRangeYBottom = rightBottomPelPosY - (isFullPelVerLuma ? 0 : LumaRBSampleOffset);

	if (!(PredXLeft >= lRangeXLeft && PredXLeft <= lRangeXRight) || !(PredXRight >= lRangeXLeft && PredXRight <= lRangeXRight))
	{
		return false;
	}
	else if (!(PredYTop >= lRangeYTop && PredYTop <= lRangeYBottom) || !(PredYBottom >= lRangeYTop && PredYBottom <= lRangeYBottom))
	{
		return false;
	}

	if ((chromaFormat != CHROMA_444) && (chromaFormat != CHROMA_400))
	{
		// Chroma MV range check
		const Bool isFullPelHorChroma = (cMv.getHor() % 8 == 0);
		const Bool isFullPelVerChroma = (cMv.getVer() % 8 == 0);

		const Int cRangeXLeft = leftTopPelPosX + (isFullPelHorChroma ? 0 : CromaLTSampleoffset);
		const Int cRangeYTop = leftTopPelPosY + (isFullPelVerChroma ? 0 : CromaLTSampleoffset);
		const Int cRangeXRight = rightBottomPelPosX - (isFullPelHorChroma ? 0 : CromaRBSampleoffset);
		const Int cRangeYBottom = rightBottomPelPosY - (isFullPelVerChroma ? 0 : CromaRBSampleoffset);

		if (!(PredXLeft >= cRangeXLeft && PredXLeft <= cRangeXRight) || !(PredXRight >= cRangeXLeft && PredXRight <= cRangeXRight))
		{
			return false;
		}
		else if ((!(PredYTop >= cRangeYTop && PredYTop <= cRangeYBottom) || !(PredYBottom >= cRangeYTop && PredYBottom <= cRangeYBottom)) && (chromaFormat != CHROMA_422))
		{
			return false;
		}
	}

	return true;
}

Bool TComPrediction::checkTMctsMvp(TComDataCU* pcCU, Int partIdx)
{
	Int   partWidth = 0;
	Int   partHeight = 0;
	UInt  partAddr = 0;

	UInt  tileXPosInCtus = 0;
	UInt  tileYPosInCtus = 0;
	UInt  tileWidthtInCtus = 0;
	UInt  tileHeightInCtus = 0;

	getTilePosition(pcCU, tileXPosInCtus, tileYPosInCtus, tileWidthtInCtus, tileHeightInCtus);

	const UInt            ctuLength = pcCU->getPic()->getPicSym()->getSPS().getMaxCUWidth();
	const ChromaFormat chromaFormat = pcCU->getPic()->getPicSym()->getSPS().getChromaFormatIdc();

	Int   predXLeft;
	Int   predYTop;
	Int   predXRight;
	Int   predYBottom;

	if (partIdx >= 0)
	{
		pcCU->getPartIndexAndSize(partIdx, partAddr, partWidth, partHeight);

		if (xCheckIdenticalMotion(pcCU, partAddr))
		{
			RefPicList eRefPicList = REF_PIC_LIST_0;
			TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(partAddr);
			getRefPUPartPos(pcCU, cMv, partIdx, predXLeft, predYTop, predXRight, predYBottom, partWidth, partHeight);
			if (!checkMVPRange(cMv, ctuLength, tileXPosInCtus, tileYPosInCtus, tileWidthtInCtus, tileHeightInCtus, predXLeft, predXRight, predYTop, predYBottom, chromaFormat))
			{
				return false;
			}
		}
		else
		{
			for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
			{
				RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);

				TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(partAddr);
				getRefPUPartPos(pcCU, cMv, partIdx, predXLeft, predYTop, predXRight, predYBottom, partWidth, partHeight);
				if (!checkMVPRange(cMv, ctuLength, tileXPosInCtus, tileYPosInCtus, tileWidthtInCtus, tileHeightInCtus, predXLeft, predXRight, predYTop, predYBottom, chromaFormat))
				{
					return false;
				}
			}
		}
		return true;
	}

	for (partIdx = 0; partIdx < pcCU->getNumPartitions(); partIdx++)
	{
		pcCU->getPartIndexAndSize(partIdx, partAddr, partWidth, partHeight);

		if (xCheckIdenticalMotion(pcCU, partAddr))
		{
			RefPicList eRefPicList = REF_PIC_LIST_0;
			TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(partAddr);
			getRefPUPartPos(pcCU, cMv, partIdx, predXLeft, predYTop, predXRight, predYBottom, partWidth, partHeight);
			if (!checkMVPRange(cMv, ctuLength, tileXPosInCtus, tileYPosInCtus, tileWidthtInCtus, tileHeightInCtus, predXLeft, predXRight, predYTop, predYBottom, chromaFormat))
			{
				return false;
			}
		}
		else
		{
			for (UInt refList = 0; refList < NUM_REF_PIC_LIST_01; refList++)
			{
				RefPicList eRefPicList = (refList ? REF_PIC_LIST_1 : REF_PIC_LIST_0);

				TComMv      cMv = pcCU->getCUMvField(eRefPicList)->getMv(partAddr);
				getRefPUPartPos(pcCU, cMv, partIdx, predXLeft, predYTop, predXRight, predYBottom, partWidth, partHeight);
				if (!checkMVPRange(cMv, ctuLength, tileXPosInCtus, tileYPosInCtus, tileWidthtInCtus, tileHeightInCtus, predXLeft, predXRight, predYTop, predYBottom, chromaFormat))
				{
					return false;
				}
			}
		}
	}
	return true;
}



#endif
//! \}
