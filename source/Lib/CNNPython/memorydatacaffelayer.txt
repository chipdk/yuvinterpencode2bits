layers {
  name: "data"
  type: MEMORY_DATA
  top: "data"
  top: "label"
  memory_data_param {
    batch_size: 1
    channels: 1
    height: 1160
    width: 2000
  }
}



layer {
  name: "data"
  type: "HDF5Data"
  top: "data"
  top: "label"
  hdf5_data_param {
    source: "/home/chipdk/CNN_interpolating_model/VDSR_test.txt"
    batch_size: 2
  }
  include: { phase: TEST }
}


layer {
  name: "data"
  type: "Input"
  top: "data"
  input_param { shape: { dim: 10 dim: 3 dim: 227 dim: 227 } }
}
